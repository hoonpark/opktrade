#include "stdafx.h"

#include <sys/timeb.h>
#include <time.h>

#include "debug/DebugLogDlg.h"

CDebugLogView* CDebugLog::m_pDebugLogView;
CDebugLogView* CDebugLog::m_pTradeLogView;
CDebugLog DebugLog;

string & CDebugLog::GetTimeStamp()
{
	static string log;
	struct timeb timebuffer;

	ftime( &timebuffer );

	char buffer[80];
	strftime( buffer, sizeof( buffer ), "%H:%M:%S", localtime( &timebuffer.time ) );

	string strBuff( buffer );
	string milsec = to_string( timebuffer.millitm + 1000 ).substr( 1, 3 );

	log = "[" + strBuff + "." + milsec + "] ";

	return log;
}

void CDebugLog::Write( const string & logText )
{
	string log = GetTimeStamp() + logText;

	CDebugLog::m_pDebugLogView->AddLog( log );

	ofstream outfile( sLogFilePath, std::ofstream::out | std::ofstream::app );
	outfile.seekp( outfile.end );
	outfile.write( log.c_str(), log.size() );
	outfile.close();
}

void CDebugLog::HiddenWrite( const string & logText )
{
	string log = GetTimeStamp() + logText;

	//CDebugLog::m_pDebugLogView->AddLog( log );

	ofstream outfile( sHiddenLogFilePath, std::ofstream::out | std::ofstream::app );
	outfile.seekp( outfile.end );
	outfile.write( log.c_str(), log.size() );
	outfile.close();
}

void CDebugLog::TradeLog( const string & logText )
{
	string log = GetTimeStamp() + logText;

	CDebugLog::m_pTradeLogView->AddLog( log );

	ofstream outfile( sTradeLogFilePath, std::ofstream::out | std::ofstream::app );
	outfile.seekp( outfile.end );
	outfile.write( log.c_str(), log.size() );
	outfile.close();
}
