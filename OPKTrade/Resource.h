//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// OPKTrade.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_OPKTRADE_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_DLG_DEBUG_LOG               130
#define IDC_STC_MONITORING              1002
#define IDC_STATIC_TITLE1               1003
#define IDC_STATIC_TITLE2               1004
#define IDC_STATIC_TITLE1_1             1005
#define IDC_STATIC_TITLE1_2             1006
#define IDC_STATIC_TITLE2_1             1007
#define IDC_STATIC_TITLE2_2             1008
#define IDC_STATIC_DATA1_1              1009
#define IDC_STATIC_DATA1_2              1010
#define IDC_STATIC_DATA2_1              1011
#define IDC_STATIC_DATA2_2              1012
#define IDC_STATIC_RATE1                1013
#define IDC_STATIC_TITLE1_3             1014
#define IDC_STATIC_TITLE2_3             1015
#define IDC_STATIC_DATA1_3              1016
#define IDC_STATIC_DATA2_3              1017
#define IDC_STATIC_RATE2                1018
#define IDC_STATIC_RATE3                1019
#define IDC_BTN_START_TRADE             1022
#define IDC_BTN_DBG_LOG                 1023
#define IDC_LIST_LOG                    1024
#define IDC_BTN_WRITE2FILE              1025
#define IDC_BTN_CLEARLOG                1026
#define IDC_LIST_LOGVIEW                1026
#define IDC_LIST_TRADELOGVIEW           1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
