#pragma once

class COrderBookElement {
public:
	double m_dSize;
	double m_dRate;
};

class CMarketOrderBook {
public:
	CMarketOrderBook();

public:

	vector<COrderBookElement> m_vBTCBuyOrders;
	vector<COrderBookElement> m_vBTCSellOrders;

	vector<COrderBookElement> m_vETHBuyOrders;
	vector<COrderBookElement> m_vETHSellOrders;

	vector<COrderBookElement> m_vUSDTBuyOrders;
	vector<COrderBookElement> m_vUSDTSellOrders;

public:
	void ClearOrders();
};
