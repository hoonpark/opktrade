#include "stdafx.h"
#include "analyzer.h"
#include "receiver/BittrexReceiver.h"
#include "monitoring/MonitoringDlg.h"
#include "scheduler/ExchangeManager.h"

CAnalyzer::CAnalyzer()
{
	m_absMarketList = make_shared<CAbsMarketList>();
}

CAnalyzer::~CAnalyzer()
{

}

void CAnalyzer::GetPrimePrice()
{
	m_dBTC2ETHRate = m_receiver->GetMarketPrice( "BTC-ETH" );
	m_dBTC2USDTRate = m_receiver->GetMarketPrice( "USDT-BTC" );
	m_dETH2USDTRate = m_receiver->GetMarketPrice( "USDT-ETH" );
}

void CAnalyzer::InitializeMarketList()
{
	if( !m_absMarketList->IsInitialized() ){
		//m_receiver->GetMarketList( *m_absMarketList );
		shared_ptr<CPairExchange> receiver = m_vecPairExchange.at(0);
		receiver->GetMarketList(*m_absMarketList);

	}
}

size_t curl_test(void *content, size_t size, size_t nmemb, std::string *buffer)
{
	
	buffer->append((char*)content, size*nmemb);

	return size * nmemb;
}

//#include "lib/libcurl/include/curl.h"
//#include "lib/jsoncpp/include/json.h"
void CAnalyzer::run()
{
	m_vecPairExchange.push_back(CExchangeFactory::CreateInstance("bittrex"));
	m_vecPairExchange.push_back(CExchangeFactory::CreateInstance("binance"));
	InitializeMarketList();

	//CURL* m_curl = nullptr;
	//m_curl = curl_easy_init();

	//vector <string> v;
	//string action = "POST";
	//string post_data = "";

	////Json::Value result;
	//string str_result;

	////string url = "https://allbit.com/open/coin-list/";
	////url += "/open/coin-amount?addr=0x1836581c960056e7703110c3eb3d7d205a88bec1&token=14d7cb6285b9e71e6645a58917a373949d0aca2eed923456b5e4440771ccbc3714d7cb6285b9e71e6645a58917a373949d0aca2eed923456b5e4440771ccbc37";
	////string url = "https://allbit.com/open/order-list/";
	//string url = "https://allbit.com/open/coin-amount/";

	////v.push_back("coin:4");
	//{

	//	CURLcode res;

	//	if (m_curl) {

	//		curl_easy_setopt(m_curl, CURLOPT_URL, url.c_str());
	//		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, curl_test);
	//		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &str_result);
	//		//curl_easy_setopt(m_curl, CURLOPT_SSL_VERIFYPEER, false);
	//		//curl_easy_setopt(BinaCPP::curl, CURLOPT_ENCODING, "gzip");

	//		if (v.size() > 0) {

	//			struct curl_slist *chunk = NULL;
	//			for (int i = 0; i < v.size(); i++) {
	//				chunk = curl_slist_append(chunk, v[i].c_str());
	//			}
	//			curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, chunk);
	//		}

	//		if (post_data.size() > 0 || action == "POST" || action == "PUT" || action == "DELETE") {

	//			if (action == "PUT" || action == "DELETE") {
	//				curl_easy_setopt(m_curl, CURLOPT_CUSTOMREQUEST, action.c_str());
	//			}
	//			//curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, post_data.c_str());
	//			//curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "&coin=4");
	//			curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, "&addr=0x1836581c960056e7703110c3eb3d7d205a88bec1&token=14d7cb6285b9e71e6645a58917a373949d0aca2eed923456b5e4440771ccbc3714d7cb6285b9e71e6645a58917a373949d0aca2eed923456b5e4440771ccbc37");
	//		}

	//		res = curl_easy_perform(m_curl);

	//		int tetst = 0;
	//		tetst = 1;
	//		/* Check for errors */
	//		if (res != CURLE_OK) {
	//			"error";
	//		}

	//	}
	//}
	//	

	//curl_easy_cleanup(m_curl);
}

void CAnalyzer::stop()
{
	m_receiver->Stop();
}

string & CAnalyzer::GetSuggestReport()
{
	// TODO: 여기에 반환 구문을 삽입합니다.
	return m_absMarketList->GetSuggestReport();
}

int CAnalyzer::UpdateMarketStatus()
{
	//return m_receiver->GetMarketSummary( *m_absMarketList );
	return m_vecPairExchange.at(0)->GetMarketSummary(*m_absMarketList);
}

double CAnalyzer::SimpleCheckYield()
{
	auto nSize = UpdateMarketStatus();

	double dResult = 0;
	if( nSize > 0 )
		dResult = m_absMarketList->CalcYields();

	return dResult;
}

vector<pair<string, bool>> CAnalyzer::GetOrderBookRequest( const double dYieldCap )
{
	static mutex m_mtxOrderBookRequest;
	lock_guard<mutex> sync( m_mtxOrderBookRequest );

	return m_absMarketList->GetOrderBookRequest( dYieldCap );
}
