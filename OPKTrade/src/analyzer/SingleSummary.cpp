#include "stdafx.h"

#include "SingleSummary.h"

CMarketOrderBook::CMarketOrderBook()
{
	m_vBTCBuyOrders.reserve( 10 );
	m_vBTCSellOrders.reserve( 10 );

	m_vETHBuyOrders.reserve( 10 );
	m_vETHSellOrders.reserve( 10 );

	m_vUSDTBuyOrders.reserve( 10 );
	m_vUSDTSellOrders.reserve( 10 );
}

void CMarketOrderBook::ClearOrders()
{
	m_vBTCBuyOrders.clear();
	m_vBTCSellOrders.clear();
	m_vETHBuyOrders.clear();
	m_vETHSellOrders.clear();
	m_vUSDTBuyOrders.clear();
	m_vUSDTSellOrders.clear();
}
