#pragma once

#include "SingleSummary.h"

class CReceiver;
class CAbsMarketList;
class CPairExchange;
class CAnalyzer
{
protected:
	string m_totalResult;
	vector<CMarketOrderBook> m_vOrderBook;

public:
	double m_dBTC2ETHRate{ 0.0 };
	double m_dBTC2USDTRate{ 0.0 };
	double m_dETH2USDTRate{ 0.0 };

	void GetPrimePrice();

	

public:
	CAnalyzer();
	virtual ~CAnalyzer();

	void InitializeMarketList();
	void run();
	void stop();

	string& GetResult() {
		return m_totalResult;
	}

	//string& GetMarketOrderBook( const string& sMarketName );

	string& GetSuggestReport();

protected:

public:
	int UpdateMarketStatus();
	double SimpleCheckYield();

	// Test Code for OrderBook Request
	vector<pair<string, bool>> GetOrderBookRequest( const double dYieldCap );

private:
	shared_ptr<CReceiver> m_receiver;
	//CBittrexReceiver* 
	shared_ptr<CAbsMarketList> m_absMarketList;

	vector<shared_ptr<CPairExchange>> m_vecPairExchange;
};
