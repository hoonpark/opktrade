#pragma once

class CThreadOperatorBase;
class CSender
{
public:
	CSender();

protected:
	shared_ptr<CThreadOperatorBase> m_threadTrader;
};
