#include "stdafx.h"
#include "BittrexSender.h"
#include "../thread/ThreadOperatorBittrex.h"
#include "../src/monitoring/MonitoringDlg.h"
#include "../scheduler/Scheduler.h"

CBittrexSender::CBittrexSender() : CSender()
{
	m_threadTrader = make_shared<CBittrexTrader>("626c1c8a8f5547f989d4cae6e3149dd7", "58567f510782488c86ea3e46e3672647");

	CMonitoringDlg::InsertThreadBase(CMonitoringDlg::TRADE, m_threadTrader);

	shared_ptr<CBittrexTrader> trader = dynamic_pointer_cast<CBittrexTrader>(m_threadTrader);

	trader->SetCallbackHandler(bind(&CScheduleManager::TradeCallback, CScheduleManager::GetInstance(), placeholders::_1));

	m_threadTrader->RunThread();
}