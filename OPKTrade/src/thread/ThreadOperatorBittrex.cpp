#include "stdafx.h"
#include "ThreadOperatorBittrex.h"
#include "scheduler/Scheduler.h"
#include "scheduler/BufferManager.h"
#include "../../define.h"


std::time_t getEpochTime(const std::string& dateTime)
{
	static const std::string dateTimeFormat{ "%Y-%m-%dT%H:%M:%SZ" };

	std::istringstream ss{ dateTime };

	std::tm dt;
	ss >> std::get_time(&dt, dateTimeFormat.c_str());

	return std::mktime(&dt);
}

ThreadOperatorBittrex::ThreadOperatorBittrex(const string &key, const string &secret) : CThreadOperatorBase(), m_client(key, secret), m_public(m_client.get_public())
, m_market(m_client.get_market())
, m_account(m_client.get_account())
{
}

ThreadOperatorBittrex::~ThreadOperatorBittrex()
{

}


CBittrexMarketListUpdater::CBittrexMarketListUpdater(const string &key, const string &secret) : ThreadOperatorBittrex(key, secret)
{

}

void CBittrexMarketListUpdater::GetMarkets(List<response::Market> &markets)
{
	lock_guard<mutex> sync(m_mtx);
	markets = m_markets;
}

void CBittrexMarketListUpdater::Run()
{
	while (1)
	{
		{
			lock_guard<mutex> sync(m_mtx);
			m_markets.clear();
			m_markets = m_public.get_markets();
			AfxGetApp()->PostThreadMessage(WM_USER_UPDATE_MARKET_LIST, 0, 0);
		}
		Sleep(50);
	}
}

CBittrexMarketSummariesUpdater::CBittrexMarketSummariesUpdater(const string &key, const string &secret) : ThreadOperatorBittrex(key, secret)
{

}

void CBittrexMarketSummariesUpdater::GetSummary(vector<CAbsMarketSummary> &summaries)
{
	lock_guard<mutex> sync(m_mtx);
	m_summaries = summaries;
}

void CBittrexMarketSummariesUpdater::Run()
{
	while (1)
	{
		{
			lock_guard<mutex> sync(m_mtx);
			List<response::MarketSummary> tempSummaries;
			tempSummaries.clear();
			m_summaries.clear();
			tempSummaries = m_public.get_market_summaries();
			for( auto& summary : tempSummaries ){
				CAbsMarketSummary tempSummary;
				tempSummary.ask = ( double ) summary.ask;
				tempSummary.bid = ( double ) summary.bid;
				tempSummary.last = (double) summary.last;
				tempSummary.market_name = (string) summary.market_name;
				tempSummary.time_stamp = (string) summary.time_stamp;

				m_summaries.push_back( tempSummary );
			}

			CBufferManager::GetInstance()->GetSummaryBuffer()->WriteSummaries(m_summaries);
			//CSummaryBuffer::WriteSummaries(m_summaries);
			AfxGetApp()->PostThreadMessage(WM_USER_UPDATE_MARKET_SUMMARIES, 0, 0);
		}
		Sleep(50);
	}
}

CBittrexMarketOrderBookUpdater::CBittrexMarketOrderBookUpdater(const string &key, const string &secret, const string &marketName) : ThreadOperatorBittrex(key, secret), m_marketName(marketName)
{

}

void CBittrexMarketOrderBookUpdater::SetMarketName(const string &marketName)
{
	lock_guard<mutex> sync(m_mtx);
	m_marketName = marketName;
}

void CBittrexMarketOrderBookUpdater::GetOrderBook(CAbsOrderBook &orderBook)
{
	lock_guard<mutex> sync(m_mtx);
	orderBook = m_orderBook;
}


void CBittrexMarketOrderBookUpdater::Run()
{
	while (1)
	{
		{
			lock_guard<mutex> sync(m_mtx);

			if (m_marketName.empty()) continue;

			string sMarketName;
			bool bBuy;
			size_t nCopyCount = 0;
			const size_t cnCopyCountMax{ 10 };

			m_orderBook.buy.reserve(cnCopyCountMax);
			m_orderBook.sell.reserve(cnCopyCountMax);

			//while ( CReceiverBuffer::GetOrderRequest(sMarketName, bBuy)) {
			while ( CBufferManager::GetInstance()->GetReceiverBuffer()->GetOrderRequest( sMarketName, bBuy )) {
				m_orderBook.buy.clear();
				m_orderBook.sell.clear();

				nCopyCount = 0;
				if (bBuy) {
					auto& orderbook = m_public.get_order_book(sMarketName, "sell").sell;
					for (auto& order : orderbook) {
						nCopyCount++;
						if ((double)order.m_size > 0.00000002) {
							m_orderBook.sell.push_back(order);
						}
						if (nCopyCount >= cnCopyCountMax) break;
					}
					//m_orderBook.sell = m_public->get_order_book( sMarketName, "sell" ).sell;
				}
				else {
					auto& orderbook = m_public.get_order_book(sMarketName, "buy").buy;
					for (auto& order : orderbook) {
						nCopyCount++;
						if ((double)order.m_size > 0.00000002) {
							m_orderBook.buy.push_back(order);
						}
						if (nCopyCount >= cnCopyCountMax) break;
					}
					//m_orderBook.buy = m_public->get_order_book( sMarketName, "buy" ).buy;
				}

				//int nIndex = COrderBookBuffer::WriteOrderBook(sMarketName, m_orderBook);
				int nIndex = CBufferManager::GetInstance()->GetOrderBookBuffer()->WriteOrderBook(sMarketName, m_orderBook);

				AfxGetApp()->PostThreadMessage(WM_USER_UPDATE_ORDER_BOOKS, (WPARAM)nIndex, 0);
			}
		}
		Sleep(50);
	}
}


CBittrexTrader::CBittrexTrader(const string &key, const string &secret) : ThreadOperatorBittrex(key, secret)
{

}

void CBittrexTrader::SetCallbackHandler(CallbackFunction func)
{
	m_callbackFunction = move(func);
}

void CBittrexTrader::Cancel()
{
	lock_guard<mutex> sync(m_mtx);

	for (auto openedOrderName : CBufferManager::GetInstance()->GetReceiverBuffer()->GetTradeRequestForOpenedOrder())
	{
		List<response::OpenOrder> openOrders = m_market.get_open_orders(openedOrderName);

		for (auto openOrder : openOrders)
		{
			//response::Order order = m_account.get_order((string)order.order_uuid);
			std::time_t time = getEpochTime((string)openOrder.opened);

			COleDateTime dateTime(time);
			SYSTEMTIME cur_time;
			::GetSystemTime(&cur_time);
			COleDateTime currentDateTime(cur_time);

			COleDateTimeSpan span = currentDateTime - dateTime;

			if ((int)span.GetTotalSeconds() > 10)
			{
				m_market.cancel((string)openOrder.order_uuid);
				//CBittrexReceiverBuffer::WriteOpenedOrder((string)order.order_uuid);
			}
		}
		Sleep(50);
	}
}

void CBittrexTrader::Run()
{
	while (1)
	{
		lock_guard<mutex> sync(m_mtx);
		//vector<CTradePlan::CTradePlanNode> vec = CReceiverBuffer::GetTradeRequest();
		vector<CTradePlan::CTradePlanNode> vec = CBufferManager::GetInstance()->GetReceiverBuffer()->GetTradeRequest();

		if (vec.size() == 3)
		{
			int index = 0;
			ostringstream ss;
			bool bSuccess = true;

			for (auto node : vec)
			{
				string log;
				// 거래하고자하는 코인의 거래명, 가격, 수량
				double size = 0.0;
				bool bFirstTradePartialProgress = false;
				if (index == 1 && bFirstTradePartialProgress)
				{
					//string market = node.m_sMarketName;
					//size_t pos = market.find( "-" );
					//string name = market.substr( pos + 1 );
					//response::Balance bal = m_account.get_balance( name );
					//size = (double) bal.available;
					//if( size == 0 ){
					//	break;
					//}
					bSuccess = true;
				}
				else if (index == 2 && bFirstTradePartialProgress) {
					size = size;
				}
				else {
					size = node.m_dSize;
				}

				ss << "start trade item: " << std::fixed << std::setprecision(8) << node.m_sMarketName << ", price: " << node.m_dPrice << ", size: " << size << ", buy: " << node.m_bBuy << endl;
				DebugLog.TradeLog(ss.str());
				ss.clear();
				ss.str("");

				json res;

				//CReceiverBuffer::UpdateTradingStatus(node, CTradePlan::eTradeStart);
				CBufferManager::GetInstance()->GetReceiverBuffer()->UpdateTradingStatus(node, CTradePlan::eTradeStart);

				if (node.m_bBuy)
					res = m_market.buy_limit(node.m_sMarketName, size, node.m_dPrice);
				else
					res = m_market.sell_limit(node.m_sMarketName, size, node.m_dPrice);

				if (index == 1 && bFirstTradePartialProgress) {
					size = size * node.m_dPrice;
				}

				// 거래가 실패했을 경우 success가 false로 옴
				if (false == res["success"])
				{
					//CReceiverBuffer::UpdateTradingStatus(node, CTradePlan::eTradeCancel);
					CBufferManager::GetInstance()->GetReceiverBuffer()->UpdateTradingStatus(node, CTradePlan::eTradeCancel);
					// 거래 실패가 일어난 거래명
					ss << "fail trade item: " << node.m_sMarketName << endl;

					string msg = res["message"];
					// 거래 실패가 일어난 이유를 알려주는 메시지...참고 사이트 https://support.bittrex.com/hc/en-us/articles/115000240791-Error-Codes-Troubleshooting-common-error-codes
					ss << "error msg: " << msg << endl;
					DebugLog.TradeLog(ss.str());
					ss.clear();
					ss.str("");
					break;
				}

				SYSTEMTIME cur_time;
				::GetSystemTime(&cur_time);
				COleDateTime startDateTime(cur_time);

				bool isOpen = true;
				string uuid = res["result"]["uuid"];

				CBufferManager::GetInstance()->GetReceiverBuffer()->WriteOrderUUID(uuid);
				m_callbackFunction(uuid);
				// 거래 요청 후에 3초동안 성사되었는지 확인한다.

				while (isOpen)
				{
					response::Order order = m_account.get_order(uuid);

					isOpen = order.is_open;

					::GetSystemTime(&cur_time);
					COleDateTime currentDateTime(cur_time);

					COleDateTimeSpan span = currentDateTime - startDateTime;
					double dLastRemain = (double)order.quantity;

					if (isOpen && (int)span.GetTotalSeconds() > 5) {
						//거래 요청 후에 3초 이상 거래가 완료되지 않으면 요청한 거래상태를 체크한다.

						if ((double)order.quantity_remaining == (double)order.quantity) {
							// 거래가 전혀 진행되지 않은 경우
							//CReceiverBuffer::UpdateTradingStatus(node, CTradePlan::eTradeCancel);
							CBufferManager::GetInstance()->GetReceiverBuffer()->UpdateTradingStatus(node, CTradePlan::eTradeCancel);
							m_market.cancel(uuid);

							bSuccess = false;
							if (index == 0) {
								if (index <= 1) {
									string market = node.m_sMarketName;
									size_t pos = market.find("-");
									string name = market.substr(pos + 1);
									response::Balance bal = m_account.get_balance(name);
									size = (double)bal.available;
									if (size > 0) {
										// 이 경우는 일부 거래가 진행된 상태
										bFirstTradePartialProgress = true;
										ss << "cancel market(Partial Trade detected): " << node.m_sMarketName
											<< " [" << order.exchange << "] : (" << size << "/" << order.quantity << "), Limit : " << order.limit
											<< " - " << order.condition
											<< endl;
										break;
									}
								}
								// plan의 첫번째 거래인 경우 전체 plan 종료
								bSuccess = false;
							}
							else if (index == 1) {
								// TODO: 두번째 거래인 경우 구매하려던 사이즈와 가격을 report
							}
							else {
								// TODO: 세번째 거래인 경우 구매하려던 사이즈와 가격을 report ==> re-balancing 해야함
							}

							ss << "cancel market: " << node.m_sMarketName
								<< " [" << order.exchange << "] : (" << order.quantity_remaining << "/" << order.quantity << "), Limit : " << order.limit
								<< " - " << order.condition
								<< endl;

							DebugLog.TradeLog(ss.str());
							ss.clear();
							ss.str("");
							break;
						}
						else if ((double)order.quantity_remaining < (double)order.quantity) {
							// 거래가 부분 진행 중인 경우
							if ((int)span.GetTotalSeconds() > 10 && (double)order.quantity_remaining > 0) {
								// 10초가 넘으면 강제 중단
								//CReceiverBuffer::UpdateTradingStatus(node, CTradePlan::eTradeCancel);
								CBufferManager::GetInstance()->GetReceiverBuffer()->UpdateTradingStatus(node, CTradePlan::eTradeCancel);
								m_market.cancel(uuid);
								ss << "cancel market: " << node.m_sMarketName
									<< " [" << order.exchange << "] : (" << order.quantity_remaining << "/" << order.quantity << "), Limit : " << order.limit
									<< " - " << order.condition
									<< endl;
								bFirstTradePartialProgress = true;

								DebugLog.TradeLog(ss.str());
								ss.clear();
								ss.str("");
								break;
							}
							else {
								if (dLastRemain > (double)order.quantity_remaining) {
									// 진행상황에 변동이 있을 경우
									ss << "waiting order to complete: " << node.m_sMarketName
										<< " [" << order.exchange << "] : (" << order.quantity_remaining << "/" << order.quantity << "), Limit : " << order.limit
										<< " - " << order.condition
										<< endl;

									DebugLog.TradeLog(ss.str());
									ss.clear();
									ss.str("");
									dLastRemain = (double)order.quantity_remaining;
								}
								continue;
							}
						}
					}
				}

				//Node 1개의 거래 완료를 알린다.
				if (bSuccess) {
					//CReceiverBuffer::UpdateTradingStatus(node, CTradePlan::eTradeComplete);
					CBufferManager::GetInstance()->GetReceiverBuffer()->UpdateTradingStatus(node, CTradePlan::eTradeComplete);
					ss << "complete trade item: " << node.m_sMarketName << endl;

					DebugLog.TradeLog(ss.str());
					ss.clear();
					ss.str("");
				}
				else {
					if (index <= 1) {
						string market = node.m_sMarketName;
						size_t pos = market.find("-");
						string name = market.substr(pos + 1);
						response::Balance bal = m_account.get_balance(name);
						size = (double)bal.available;
						if (size > 0) {

							break;
						}
					}
					break;
				}

				index++;
			}

			// 전체 노드 거래가 정상적으로 완료되었음을 알린다.
			if (index == 3) {
				ss << "complete cycle" << endl;
				DebugLog.TradeLog(ss.str());
			}

			// c:\temp\new.txt 파일에 위 로그들을 출력하는 코드

			if (bSuccess && index == 3)
				int test = 0;
		}

		Sleep(50);
	}
}

CBittrexAccountStatus::CBittrexAccountStatus(const string &key, const string &secret) : ThreadOperatorBittrex(key, secret)
{

}

void CBittrexAccountStatus::Run()
{
	while (1)
	{
		lock_guard<mutex> sync(m_mtx);

		Sleep(50);
	}
}

CBittrexOrderStatus::CBittrexOrderStatus(const string &key, const string &secret) : ThreadOperatorBittrex(key, secret)
{

}

void CBittrexOrderStatus::SetCallbackHandler(CallbackFunction func)
{
	m_callbackFunction = move(func);
}

void CBittrexOrderStatus::Run()
{
	while (1)
	{
		lock_guard<mutex> sync(m_mtx);

		if (!CBufferManager::GetInstance()->GetReceiverBuffer()->GetOrderUUID().empty())
		{
			m_callbackFunction("abcdef");
		}

		Sleep(50);
	}
}