#include "stdafx.h"
#include "ThreadOperatorBinance.h"

ThreadOperatorBinance::ThreadOperatorBinance(const string &key, const string &secret) : CThreadOperatorBase()
{
	m_client.init(key, secret);
}

ThreadOperatorBinance::~ThreadOperatorBinance()
{
	m_client.cleanup();
}

CBinanceMarketSummariesUpdater::CBinanceMarketSummariesUpdater(const string &key, const string &secret) : ThreadOperatorBinance(key, secret)
{

}

CBinanceMarketSummariesUpdater::~CBinanceMarketSummariesUpdater()
{

}

void CBinanceMarketSummariesUpdater::Run()
{
	while (1)
	{
		lock_guard<mutex> sync(m_mtx);
		Json::Value result;
		m_client.get_24hr(m_marketName.c_str(), result);
		Sleep(50);
	}
}

CBinanceMarketOrderBookUpdater::CBinanceMarketOrderBookUpdater(const string &key, const string &secret) : ThreadOperatorBinance(key, secret)
{
}

CBinanceMarketOrderBookUpdater::~CBinanceMarketOrderBookUpdater()
{
}

void CBinanceMarketOrderBookUpdater::Run()
{
	while (1)
	{
		lock_guard<mutex> sync(m_mtx);
		const unsigned int count = 10;
		Json::Value result;
		m_client.get_depth(m_marketName.c_str(), count, result);

		m_orderBook.buy.clear();
		m_orderBook.sell.clear();

		for (auto asks : result["asks"])
		{
			double price = atof(asks[0].asString().c_str());
			double qty = atof(asks[1].asString().c_str());

			m_orderBook.sell.emplace_back(CAbsOrderBookEntry(price, qty));
		}

		for (auto bids : result["bids"])
		{
			double price = atof(bids[0].asString().c_str());
			double qty = atof(bids[1].asString().c_str());
			m_orderBook.buy.emplace_back(CAbsOrderBookEntry(price, qty));
		}
		Sleep(50);
	}
}


CBinanceTrader::CBinanceTrader(const string &key, const string &secret) : ThreadOperatorBinance(key, secret)
{
}

CBinanceTrader::~CBinanceTrader()
{
}

void CBinanceTrader::Run()
{
	while (1)
	{
		lock_guard<mutex> sync(m_mtx);

		Json::Value result;
		Sleep(50);
	}
}