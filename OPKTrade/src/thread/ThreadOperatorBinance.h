#pragma once
#include "ThreadOperatorBase.h"
#include "../receiver/binance/binacpp.h"
#include "../abstract/AbsOrderBook.h"

class ThreadOperatorBinance : public CThreadOperatorBase
{
public:
	ThreadOperatorBinance(const string &key, const string &secret);
	virtual ~ThreadOperatorBinance();

protected:
	virtual void Run() = 0;
protected:
	BinaCPP m_client;
};


class CBinanceMarketSummariesUpdater : public ThreadOperatorBinance
{
public:
	CBinanceMarketSummariesUpdater(const string &key, const string &secret);
	virtual ~CBinanceMarketSummariesUpdater();
protected:
	virtual void Run() override;

private:
	string m_marketName;
};


class CBinanceMarketOrderBookUpdater : public ThreadOperatorBinance
{
public:
	CBinanceMarketOrderBookUpdater(const string &key, const string &secret);
	virtual ~CBinanceMarketOrderBookUpdater();
protected:
	virtual void Run() override;

private:
	CAbsOrderBook m_orderBook;
	string m_marketName;
};

class CBinanceTrader : public ThreadOperatorBinance
{
public:
	CBinanceTrader(const string &key, const string &secret);
	virtual ~CBinanceTrader();
protected:
	virtual void Run() override;
};