#pragma once
#include "ThreadOperatorBase.h"
#include "../abstract/AbsSummaries.h"
#include "../receiver/exchange/bittrex.h"
#include "../receiver/client.h"
#include "../receiver/lib/utils.h"
#include "../receiver/api/public.h"

class ThreadOperatorBittrex : public CThreadOperatorBase
{
public:
	ThreadOperatorBittrex(const string &key, const string &secret);
	virtual ~ThreadOperatorBittrex();

	virtual void Run() = 0;
protected:
	bittrex::Client m_client;
	bittrex::api::Public m_public;
	bittrex::api::Market m_market;
	api::Account m_account;
};

class CBittrexMarketListUpdater : public ThreadOperatorBittrex
{
public:
	CBittrexMarketListUpdater(const string &key, const string &secret);

protected:
	virtual void Run() override;

	void GetMarkets(List<response::Market> &markets);
private:
	vector<response::Market> m_markets;
};

class CBittrexMarketSummariesUpdater : public ThreadOperatorBittrex
{
public:
	CBittrexMarketSummariesUpdater(const string &key, const string &secret);

protected:
	virtual void Run() override;
	void GetSummary( vector<CAbsMarketSummary> &summaries);
private:
	vector<CAbsMarketSummary> m_summaries;
};

class CBittrexMarketOrderBookUpdater : public ThreadOperatorBittrex
{
public:
	CBittrexMarketOrderBookUpdater(const string &key, const string &secret, const string &marketName);

	void SetMarketName(const string &marketName);
	void GetOrderBook(CAbsOrderBook &orderBook);

protected:
	virtual void Run() override;
private:
	CAbsOrderBook m_orderBook;
	string m_marketName;
};

class CBittrexTrader : public ThreadOperatorBittrex
{
private:
	typedef function<void(const string &)> CallbackFunction;
public:
	CBittrexTrader(const string &key, const string &secret);

	void SetCallbackHandler(CallbackFunction func);
	
protected:
	virtual void Run() override;
	void Cancel();

private:
	CallbackFunction m_callbackFunction;
};

class CBittrexAccountStatus : public ThreadOperatorBittrex
{
public:
	CBittrexAccountStatus(const string &key, const string &secret);
protected:
	virtual void Run() override;
};

class CBittrexOrderStatus : public ThreadOperatorBittrex
{
private:
	typedef function<void(const string &)> CallbackFunction;
public:
	CBittrexOrderStatus(const string &key, const string &secret);
	void SetCallbackHandler(CallbackFunction func);
protected:
	virtual void Run() override;

private:
	CallbackFunction m_callbackFunction;
};