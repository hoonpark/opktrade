#pragma once



class CThreadOperatorBase
{
public:
	CThreadOperatorBase()
	{
	};

	virtual void RunThread()
	{
		m_updateThread = make_shared<thread>(ThreadFunction, this);
	}
protected:
	virtual void Run() = 0;

	static void ThreadFunction(CThreadOperatorBase *updater)
	{
		updater->Run();
	}

protected:
	mutex m_mtx;
	shared_ptr<thread> m_updateThread;
};
