#pragma once


// CDebugLogDlg 대화 상자

class CDebugLogDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CDebugLogDlg)

public:
	CDebugLogDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDebugLogDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_DEBUG_LOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	void AddLog( const string& sLog );

public:
	CListBox m_listboxLog;
	afx_msg void OnBnClickedBtnWrite2file();
	afx_msg void OnBnClickedBtnClearLog();
};

class CDebugLogView : public CListBox {
public:
	CDebugLogView() = default;
	virtual ~CDebugLogView() = default;

	void AddLog( const string& sLog );

};