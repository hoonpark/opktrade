// DebugLogDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "OPKTrade.h"
#include "DebugLogDlg.h"
#include "afxdialogex.h"


// CDebugLogDlg 대화 상자

IMPLEMENT_DYNAMIC(CDebugLogDlg, CDialogEx)

CDebugLogDlg::CDebugLogDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_DEBUG_LOG, pParent)
{

}

CDebugLogDlg::~CDebugLogDlg()
{
}

void CDebugLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange( pDX );
	DDX_Control( pDX, IDC_LIST_LOG, m_listboxLog );
}


BEGIN_MESSAGE_MAP(CDebugLogDlg, CDialogEx)
	ON_BN_CLICKED( IDC_BTN_WRITE2FILE, &CDebugLogDlg::OnBnClickedBtnWrite2file )
	ON_BN_CLICKED( IDC_BTN_CLEARLOG, &CDebugLogDlg::OnBnClickedBtnClearLog )
END_MESSAGE_MAP()


// CDebugLogDlg 메시지 처리기


void CDebugLogDlg::AddLog( const string & sLog )
{
	string sLogLine;

	size_t nStartPos = 0;
	size_t nLastPos = sLog.size();
	while( nStartPos < sLog.size()){
		if(( nLastPos = sLog.find( '\n', nStartPos )) != string::npos ){
			sLogLine = sLog.substr( nStartPos, nLastPos - nStartPos );
			nStartPos = nLastPos + 1;
		} else {
			sLogLine = sLog.substr( nStartPos, sLog.size() );
		}

		m_listboxLog.AddString( sLogLine.c_str() );
		m_listboxLog.SetCurSel( m_listboxLog.GetCount() - 1 );
	}
}

void CDebugLogDlg::OnBnClickedBtnWrite2file()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CDebugLogDlg::OnBnClickedBtnClearLog()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_listboxLog.ResetContent();
}

void CDebugLogView::AddLog( const string & sLog )
{
	string sLogLine;

	size_t nStartPos = 0;
	size_t nLastPos = sLog.size();
	while( nStartPos < sLog.size() ){
		if( ( nLastPos = sLog.find( '\n', nStartPos ) ) != string::npos ){
			sLogLine = sLog.substr( nStartPos, nLastPos - nStartPos );
			nStartPos = nLastPos + 1;
		} else {
			sLogLine = sLog.substr( nStartPos, sLog.size() );
		}

		AddString( sLogLine.c_str() );
		SetCurSel( GetCount() - 1 );
	}
}
