#include "stdafx.h"

#include "OrderBookBuffer.h"

void COrderBookBuffer::InitializeOrderBookQueue()
{
	m_orderBookBuffer.resize( m_mapMarketNameTable.size() );
}

map<string, size_t>& COrderBookBuffer::GetMarketNameTable()
{
	return m_mapMarketNameTable;
}

deque<CAbsOrderBook> COrderBookBuffer::GetOrderBookBufferQueue( const string & sMarketName )
{
	static deque<CAbsOrderBook> queueResult;

	lock_guard<mutex> sync( m_mtxOrderBook );

	// TODO: 2중 deep copy 일어나지 않도록 개선할 것
	queueResult.clear();
	queueResult = m_orderBookBuffer[m_mapMarketNameTable[sMarketName]];		// deep copy
	m_orderBookBuffer[m_mapMarketNameTable[sMarketName]].clear();

	return queueResult;		// deep copy
}

int COrderBookBuffer::WriteOrderBook( const string & sMarketName, CAbsOrderBook & orderBook )
{
	lock_guard<mutex> sync( m_mtxOrderBook );

	if( m_mapMarketNameTable.find( sMarketName ) != m_mapMarketNameTable.end() ){
		m_orderBookBuffer[m_mapMarketNameTable[sMarketName]].push_back( orderBook );	// copy & push
		return (int) m_mapMarketNameTable[sMarketName];
	}

	return -1;
}

void COrderBookBuffer::ClearOrderBook( const string &sMarketName )
{
	lock_guard<mutex> sync( m_mtxOrderBook );

	if( m_mapMarketNameTable.find( sMarketName ) != m_mapMarketNameTable.end() ) {
		m_orderBookBuffer[m_mapMarketNameTable[sMarketName]].clear();
	}
}