#pragma once

#include "abstract/AbsOrderBook.h"
#include "abstract/AbsSummaries.h"

class TradeRequest
{
public:
	TradeRequest(string marketName, double quantity, double rate, bool buy) : m_marketName(marketName), m_quantity(quantity), m_rate(rate), m_buy(buy)
	{

	};
public:
	string m_marketName;
	double m_quantity;
	double m_rate;
	bool   m_buy;
};

class CTradePlan{
public:
	string m_sTradePath;

	enum EPlanUpdateID {
		eTradeNoStatus, eTradeStart, eTradeCancel, eTradeComplete
	};

	class CTradePlanNode{
	public:
		string m_sTradePathName;
		string m_sMarketName;
		double m_dPrice{ 0.0 };
		double m_dSize{ 0.0 };
		bool m_bBuy{ true };

		bool m_bFilled{ false };

		EPlanUpdateID eStatus{ eTradeNoStatus };
	};
	vector<CTradePlanNode> m_vPlan;
	size_t m_nFillCheckCounter{ 0 };

	void Initialize(){
		m_vPlan.clear();
		m_nFillCheckCounter = 0;
	}
	bool IsAllFilled();
	double OptimizeTradeSize();
	void SetPathName2Node();
};

class CTradePlans : public vector<CTradePlan>{
protected:
	deque<pair<size_t, size_t>> m_pairFoundNode;
	//size_t m_nFoundNode;
	//size_t m_nFoundPlan;

	bool FindPlan( const string& sMarketName );
public:
	void UpdateOrderBook( const string& sMarketName, CAbsOrderBook & orderBook );
};

class CReceiverBuffer {
private:
	// Thread 에서 사용할 Order Book Buffer의 vector와 mutex
	//		0~2	: BTC-ETH, USDT-BTC, USDT-ETH market
	//		3~	: 그 외 Thread에서 담당할 Order Book 들

	mutex m_mtxOrderRequest;
	deque<pair<string, bool>> m_qOrderRequest;

	mutex m_mtxTradeRequest;
	deque<vector<CTradePlan::CTradePlanNode>> m_qTradeRequest;
	deque<string> m_qTradeRequestForOpenedOrder;

	mutex m_mtxOpendOrder;
	deque<string> m_openedOrder;

	CTradePlans m_TradePlan;

	mutex m_mtxOnTrading;
	vector<CTradePlan> m_vOnTrading;

	mutex m_mtxOrderUUID;
	deque<string> m_orderUUID;

public:
	vector<string> m_orderBookNameBuffer;

public:
	CReceiverBuffer();

public:
	void WriteOrderUUID(const string &uuid);
	string GetOrderUUID();

	void WriteOpenedOrder(const string &uuid);
	deque<string> GetOpenedOrder();
	void ClearOpenedOrder();

	deque<string> GetTradeRequestForOpenedOrder();
	bool GetOpenedOrderName(string &marketName);
	void DeleteFrontOpenedOrderName();

	void WriteTradeRequest(const vector<CTradePlan::CTradePlanNode> &vPlan);
	void WriteTradeRequest(const string marketName, const double quantity, const double rate, const bool buy);
	vector<CTradePlan::CTradePlanNode>& GetTradeRequest();

	void WriteOrderRequest( const string& sMarketIndex, const bool bBuy );
	bool GetOrderRequest( string& nMarketIndex, bool& bBuy );

	void SetTradePlan( CTradePlan tradePlan );
	bool IsExistPlan( string& sPlanPathName );
	bool UpdateTradePlan( const string& sMarketName, CAbsOrderBook& orderBook );

	// Trading Status Checking
	void InsertTradingStatus( CTradePlan& plan );
	void UpdateTradingStatus( CTradePlan::CTradePlanNode& planNode, CTradePlan::EPlanUpdateID nUpdateID );
	bool CheckOnTrading( const string& sPlanPathName );

public:
	string ExtractOrderBook( CAbsOrderBook& orderBook );
};
