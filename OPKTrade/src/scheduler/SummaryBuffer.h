#pragma once

#include "abstract/AbsSummaries.h"

class CSummaryBuffer {
private:
	// Thread 에서 사용할 Summary Buffer와 mutex
	mutex m_mtxMarketSummary;
	vector<CAbsMarketSummary> m_marketSummariesBuffer;

public:
	bool WriteSummaries( vector<CAbsMarketSummary> &summaries );
	vector<CAbsMarketSummary>& GetSummaries();
	void ClearSummaries();
};
