#include "stdafx.h"
#include "ReceiverBuffer.h"

#include "scheduler/BufferManager.h"

CReceiverBuffer::CReceiverBuffer()
{
}


void CReceiverBuffer::WriteOrderRequest( const string& sMarketIndex, const bool bBuy )
{
	lock_guard<mutex> sync( m_mtxOrderRequest );
	m_qOrderRequest.emplace_back( make_pair( sMarketIndex, bBuy ) );
}

bool CReceiverBuffer::GetOrderRequest( string & nMarketIndex, bool & bBuy )
{
	lock_guard<mutex> sync( m_mtxOrderRequest );

	if( m_qOrderRequest.empty() ) return false;

	auto& order = m_qOrderRequest.front();

	nMarketIndex = order.first;
	bBuy = order.second;

	m_qOrderRequest.pop_front();

	return true;
}

void CReceiverBuffer::SetTradePlan( CTradePlan tradePlan )
{
	for( auto iter = m_TradePlan.begin(); iter != m_TradePlan.end(); ++ iter ){
		if( iter->m_sTradePath == tradePlan.m_sTradePath ){
			*iter = tradePlan;
			//m_TradePlan.erase( iter );
			return;
		}
	}

	m_TradePlan.emplace_back( tradePlan );
}

bool CReceiverBuffer::IsExistPlan( string & sPlanPathName )
{
	const size_t cnCounterLimit{ 10 };

	for( auto& plan : m_TradePlan ){
		if( plan.m_sTradePath == sPlanPathName ){
			plan.m_nFillCheckCounter++;
			if( plan.m_nFillCheckCounter > cnCounterLimit ){
				for( auto& node : plan.m_vPlan ){
					if( !node.m_bFilled ){
						CReceiverBuffer::WriteOrderRequest( node.m_sMarketName, node.m_bBuy );
					}
				}
				plan.m_nFillCheckCounter = 0;
			}

			return true;
		}
	}

	return false;
}

bool CReceiverBuffer::UpdateTradePlan( const string& sMarketName, CAbsOrderBook & orderBook )
{
	m_TradePlan.UpdateOrderBook( sMarketName, orderBook );

	//WriteTradeRequest;
	return false;
}


vector<CTradePlan::CTradePlanNode>& CReceiverBuffer::GetTradeRequest()
{
	static vector<CTradePlan::CTradePlanNode> vec;
	lock_guard<mutex> sync(m_mtxTradeRequest);
	
	vec.clear();

	if (m_qTradeRequest.empty()) return vec;

	auto& trade = m_qTradeRequest.front();
	vec.swap(trade);

	m_qTradeRequest.pop_front();

	return vec;

}

deque<string> CReceiverBuffer::GetOpenedOrder()
{
	static deque<string> uuids;
	lock_guard<mutex> sync(m_mtxOpendOrder);
	
	uuids = m_openedOrder;

	return uuids;
}

void CReceiverBuffer::ClearOpenedOrder()
{
	lock_guard<mutex> sync(m_mtxOpendOrder);
	m_openedOrder.clear();
}

void CReceiverBuffer::WriteOpenedOrder(const string &uuid)
{
	lock_guard<mutex> sync(m_mtxOpendOrder);
	if (std::find(m_openedOrder.begin(), m_openedOrder.end(), uuid) == m_openedOrder.end())
		m_openedOrder.emplace_back(uuid);
}


void CReceiverBuffer::DeleteFrontOpenedOrderName()
{
	lock_guard<mutex> sync(m_mtxTradeRequest);

	m_qTradeRequestForOpenedOrder.pop_front();
}

bool CReceiverBuffer::GetOpenedOrderName(string &marketName)
{
	lock_guard<mutex> sync(m_mtxTradeRequest);

	if (m_qTradeRequestForOpenedOrder.empty()) return false;

	marketName = m_qTradeRequestForOpenedOrder.front();

	return true;
}

deque<string> CReceiverBuffer::GetTradeRequestForOpenedOrder()
{
	static deque<string> openedOrder;

	lock_guard<mutex> sync(m_mtxTradeRequest);

	openedOrder = m_qTradeRequestForOpenedOrder;
	return openedOrder;
}

void CReceiverBuffer::WriteTradeRequest(const vector<CTradePlan::CTradePlanNode> &vPlan)
{
	lock_guard<mutex> sync(m_mtxTradeRequest);
	vector<CTradePlan::CTradePlanNode> v;
	v.resize(vPlan.size());
	v = vPlan;
	m_qTradeRequest.emplace_back(v);

	for (auto node : vPlan)
	{
		WriteTradeRequest(node.m_sMarketName, node.m_dSize, node.m_dPrice, node.m_bBuy);
	}
}
void CReceiverBuffer::WriteTradeRequest(const string marketName, const double quantity, const double rate, const bool buy)
{
	//lock_guard<mutex> sync(m_mtxTradeRequest);

	//m_qTradeRequest.emplace_back( TradeRequest(marketName, quantity, rate, buy) );
	if (std::find(m_qTradeRequestForOpenedOrder.begin(), m_qTradeRequestForOpenedOrder.end(), marketName) == m_qTradeRequestForOpenedOrder.end())
		m_qTradeRequestForOpenedOrder.emplace_back(marketName);
}

void CReceiverBuffer::InsertTradingStatus( CTradePlan & plan )
{
	lock_guard<mutex> sync( m_mtxOnTrading );

	m_vOnTrading.push_back( plan );
}

void CReceiverBuffer::UpdateTradingStatus( CTradePlan::CTradePlanNode & planNode, CTradePlan::EPlanUpdateID nUpdateID )
{
	lock_guard<mutex> sync( m_mtxOnTrading );

	for( auto iter = m_vOnTrading.begin(); iter != m_vOnTrading.end(); ++iter ){
		if( iter->m_sTradePath == planNode.m_sTradePathName ){
			bool bStatusCheck = false;
			int nCompleteCount = 0;

			for( auto& node : iter->m_vPlan ){
				if( node.m_sMarketName == planNode.m_sMarketName ){
					node.eStatus = nUpdateID;
				}

				if( node.eStatus == CTradePlan::eTradeCancel ){
					bStatusCheck = true;
					break;
				} else if( node.eStatus == CTradePlan::eTradeComplete ){
					nCompleteCount++;
				} else {
					bStatusCheck = false;
				}
			}
			if( nCompleteCount == iter->m_vPlan.size() )
				bStatusCheck = true;

			if( bStatusCheck ){
				m_vOnTrading.erase( iter );
			}

			break;
		}
	}
}

bool CReceiverBuffer::CheckOnTrading( const string & sPlanPathName )
{
	lock_guard<mutex> sync( m_mtxOnTrading );

	for( auto& plan : m_vOnTrading ){
		if( plan.m_sTradePath == sPlanPathName ){
			return true;
		}
	}

	return false;
}

string CReceiverBuffer::ExtractOrderBook( CAbsOrderBook & orderBook )
{
	string result;

	{
		const int nOrderReportLimit = 2;

		for( int i = 0; i < orderBook.sell.size(); i++ )
		{
			if( i > nOrderReportLimit ) break;
			CAbsOrderBookEntry entry = orderBook.sell.at( i );
			result += "sell: " + to_string( i + 1 ) + ": " + to_string( entry.m_size ) + ", " + to_string( entry.m_price ) + "\n";
		}

		for( int i = 0; i < orderBook.buy.size(); i++ )
		{
			if( i > nOrderReportLimit ) break;
			CAbsOrderBookEntry entry = orderBook.buy.at( i );
			result += "buy: " + to_string( i + 1 ) + ": " + to_string( entry.m_size ) + ", " + to_string( entry.m_price ) + "\n";
		}
	}

	return result;
}

bool CTradePlans::FindPlan( const string & sMarketName )
{
	m_pairFoundNode.clear();
	for( size_t i = 0; i < size(); ++i ){
		size_t j = 0;
		for( auto& node : at( i ).m_vPlan ){
			if( node.m_sMarketName == sMarketName ){
				m_pairFoundNode.push_back( make_pair( i, j ) );
				//m_nFoundPlan = i;
				//m_nFoundNode = j;
				return true;
			}
			j++;
		}
	}

	return false;
}
#include "../../Debug.h"
void CTradePlans::UpdateOrderBook( const string & sMarketName, CAbsOrderBook & orderBook )
{
	bool bOrderLog = false;
	if( FindPlan( sMarketName ) ){
		while( m_pairFoundNode.size() > 0 ){
			auto foundIndex = m_pairFoundNode.front();
			auto& plan = at( foundIndex.first );
			auto& node = plan.m_vPlan[foundIndex.second];
			m_pairFoundNode.pop_front();

			const int nOrderReportLimit = 2;

			if( orderBook.buy.size() > 0 && node.m_bBuy == false ){
				node.m_dPrice = (double) orderBook.buy[0].m_price;
				node.m_dSize = (double) orderBook.buy[0].m_size;
				node.m_bBuy = false;

				if( bOrderLog )
				{
					for( int j = 0; j < orderBook.buy.size(); j++ )
					{
						ostringstream ss;
						if( j > nOrderReportLimit ) break;
						ss << std::fixed << std::setprecision( 8 ) << "marknet: " << sMarketName << ", buy quantity: " << orderBook.buy.at( j ).m_size << ", buy rate: " << orderBook.buy.at( j ).m_price << endl;
						DebugLog.Write( ss.str() );
					}
				}
				node.m_bFilled = true;

			} else if( orderBook.sell.size() > 0 && node.m_bBuy == true ){
				node.m_dPrice = (double) orderBook.sell[0].m_price;
				node.m_dSize = (double) orderBook.sell[0].m_size;
				node.m_bBuy = true;

				if( bOrderLog )
				{
					for( int j = 0; j < orderBook.sell.size(); j++ )
					{
						ostringstream ss;
						if( j > nOrderReportLimit ) break;
						ss << std::fixed << std::setprecision( 8 ) << "marknet: " << sMarketName << ", sell quantity: " << orderBook.sell.at( j ).m_size << ", sell rate: " << orderBook.sell.at( j ).m_price << endl;
						DebugLog.Write( ss.str() );
					}
				}
				node.m_bFilled = true;

			}


			if( plan.IsAllFilled() ){
				if( plan.OptimizeTradeSize() > 0 ){

					CBufferManager::GetInstance()->GetReceiverBuffer()->WriteTradeRequest(plan.m_vPlan);
					CBufferManager::GetInstance()->GetReceiverBuffer()->InsertTradingStatus(plan);
					//CReceiverBuffer::WriteTradeRequest( plan.m_vPlan );
					//CReceiverBuffer::InsertTradingStatus( plan );
				}

				at( foundIndex.first ) = at( size() - 1 );
				resize( size() - 1 );

				/*
				for( auto& node : plan.m_vPlan ){
					CReceiverBuffer::WriteTradeRequest( node.m_sMarketName, node.m_dPrice, node.m_dSize, node.m_bBuy );
				}
				*/
			}
		}
	}
}

bool CTradePlan::IsAllFilled()
{
	bool bResult = true;

	for( auto& node : m_vPlan ){
		bResult &= node.m_bFilled;
	}
	if( bResult ){
		for( auto& node : m_vPlan ){
			node.m_bFilled = false;
		}
	}

	return bResult;
}

double CTradePlan::OptimizeTradeSize()
{
	stringstream sLog;
	double sTradeRate = 0;
	double dBuyComm = 0.0025;
	double dSellComm = 0.0025;

	enum ECapType {
		eETHCap, eBTCCap, eUSDTCap
	};
	const string sETH{ "ETH" };
	const string sBTC{ "BTC" };
	const string sUSDT{ "USDT" };
	auto GetStartMarket = []( const string& sMarketName )->ECapType {
		if( sMarketName.find( "ETH" ) != string::npos )	return eETHCap;
		if( sMarketName.find( "BTC" ) != string::npos )	return eBTCCap;
		return eUSDTCap;
	};
	auto GetCapValue = []( const ECapType nCapType ) -> double {
		switch( nCapType ){
		case eETHCap:		return 0.1;
		case eBTCCap:		return 0.01;
		default:			return 50.0;
		}
	};
	auto GetMinCapValue = []( const ECapType nCapType ) -> double {
		switch( nCapType ){
		case eETHCap:		return 0.0005;
		case eBTCCap:		return 0.0005;
		default:			return 0.05;
		}
	};
	auto GetBaseMarketName = []( const ECapType nCapType ) -> string {
		switch( nCapType ){
		case eETHCap:		return "ETH";
		case eBTCCap:		return "BTC";
		default:			return "USDT";
		}
	};

	ECapType nCapType = GetStartMarket( m_vPlan[0].m_sMarketName );
	ECapType nCapType2 = GetStartMarket( m_vPlan[1].m_sMarketName );
	double dCap = GetCapValue( nCapType );
	double dMinCap = GetMinCapValue( nCapType ); 

	SetPathName2Node();
	sLog << m_sTradePath << " Optimizing Processing..." << endl;

	string sOptimizingLogPrev[3];
	size_t nIndex = 0;
	for( auto& plan : m_vPlan ){
		stringstream sLogPrev;
		sLogPrev.str( "" );
		sLogPrev << setw( 16 ) << setprecision( 8 ) << fixed << plan.m_dSize;
		sOptimizingLogPrev[nIndex++] = sLogPrev.str();
	}

	if( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) > dCap ){
		m_vPlan[0].m_dSize = dCap / m_vPlan[0].m_dPrice;
	}

	// Size Optimizing
	if( m_vPlan[0].m_dSize > m_vPlan[1].m_dSize ){
		m_vPlan[0].m_dSize = m_vPlan[1].m_dSize;
	} else if( m_vPlan[0].m_dSize < m_vPlan[1].m_dSize ){
		m_vPlan[1].m_dSize = m_vPlan[0].m_dSize;
	}

	if( m_vPlan[2].m_bBuy ){
		if( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) < ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) ){
			double dNewSize = ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) / m_vPlan[1].m_dPrice;
			m_vPlan[0].m_dSize = m_vPlan[1].m_dSize = dNewSize;
		} else {
			m_vPlan[2].m_dSize = ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) / m_vPlan[2].m_dPrice;
		}
		sTradeRate = ( m_vPlan[2].m_dSize ) - ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice )
			- ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) * dBuyComm )
			- ( ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) * dSellComm ) / m_vPlan[2].m_dPrice )
			- ( ( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) * dBuyComm ) / m_vPlan[2].m_dPrice )
			;
	} else {
		if( ( m_vPlan[2].m_dSize ) < ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) ){
			double dNewSize = ( m_vPlan[2].m_dSize ) / m_vPlan[1].m_dPrice;
			m_vPlan[0].m_dSize = m_vPlan[1].m_dSize = dNewSize;
		} else {
			m_vPlan[2].m_dSize = ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice );
		}
		sTradeRate = ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) - ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice )
			- ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) * dBuyComm )
			- ( ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) * dSellComm ) * m_vPlan[2].m_dPrice )
			- ( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) * dBuyComm )
			;
	}

	nIndex = 0;
	for( auto& plan : m_vPlan ){
		sLog << " [" << plan.m_sMarketName << "] : " << ( plan.m_bBuy ? "BUY" : "SELL" ) << " : "
			<< sOptimizingLogPrev[nIndex++] << " --> " << setw( 16 ) << setprecision( 8 ) << fixed << plan.m_dSize << " (" << plan.m_dPrice << ")" << endl;
	}
	sLog << "-Respect Rate : " << sTradeRate << " " << m_vPlan[0].m_sMarketName.substr( 0, 3 ) << endl;

	stringstream sLogMinimum;
	bool bMinimumNotMet = true;
	double dMinimumPrice = 0;
	ECapType nMinimumType = eETHCap;
	
	if( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) < GetMinCapValue( nCapType ) ){
		dMinimumPrice = m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice;
		nMinimumType = nCapType;
		sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[0].m_dSize << " (" << m_vPlan[0].m_dPrice << ") ";
	} else if(  ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) < GetMinCapValue( nCapType2 ) ) {
		dMinimumPrice = m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice;
		nMinimumType = nCapType2;
		sLogMinimum << "2nd Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[1].m_dSize << " (" << m_vPlan[1].m_dPrice << ") ";
	//} else if( nCapType == eETHCap && ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) < dMinCap ) ){
	//	dMinimumPrice = m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice;
	//	nMinimumType = eETHCap;
	//	sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[0].m_dSize << " (" << m_vPlan[0].m_dPrice << ") ";
	//} else if( nCapType2 == eETHCap && ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) < GetMinCapValue( nCapType2 ) ) ){
	//	dMinimumPrice = m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice;
	//	nMinimumType = eETHCap;
	//	sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[1].m_dSize << " (" << m_vPlan[1].m_dPrice << ") ";
	} else {
		bMinimumNotMet = false;
	}
	if( bMinimumNotMet ){
		sLog.str( "" );
		sLog << m_sTradePath << " Optimizing Processing... ";
		sLog << "Min-Trade Detected : " << sLogMinimum.str() << "- " << dMinimumPrice << ( nMinimumType == eBTCCap ? " BTC" : " ETH" ) << endl;
		DebugLog.Write( sLog.str() );
		return -1.0;
	}


	DebugLog.Write( sLog.str() );

	return sTradeRate;
}

void CTradePlan::SetPathName2Node()
{
	for( auto& node : m_vPlan ){
		node.m_sTradePathName = m_sTradePath;
	}
}


void CReceiverBuffer::WriteOrderUUID(const string &uuid)
{
	lock_guard<mutex> sync(m_mtxOrderUUID);

	m_orderUUID.emplace_back(uuid);
}

string CReceiverBuffer::GetOrderUUID()
{
	lock_guard<mutex> sync(m_mtxOrderUUID);

	if (m_orderUUID.empty()) return "";

	auto uuid = m_orderUUID.front();

	m_orderUUID.pop_front();
	return uuid;
}