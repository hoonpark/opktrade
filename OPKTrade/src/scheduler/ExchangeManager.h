#pragma once

class CReceiver;
class CSender;
class CAbsMarketList;
class CPairExchange
{
public:
	CPairExchange();

	virtual int GetMarketList(CAbsMarketList& absMarketList);
	virtual int GetMarketSummary(CAbsMarketList& absMarketList);

protected:
	shared_ptr<CReceiver> m_receiver;
	shared_ptr<CSender>   m_sender;
};

class CPairBittrexExchange : public CPairExchange
{
public:
	CPairBittrexExchange();
};

class CPairBinanceExchange : public CPairExchange
{
public:
	CPairBinanceExchange();
};


class CExchangeFactory
{
public:
	CExchangeFactory();

	static shared_ptr<CPairExchange> CreateInstance(const string exchange);
};