#pragma once

class CAbsOrderBook;
class TradeRequest
{
public:
	TradeRequest(string marketName, double quantity, double rate, bool buy) : m_marketName(marketName), m_quantity(quantity), m_rate(rate), m_buy(buy)
	{

	};
public:
	string m_marketName;
	double m_quantity;
	double m_rate;
	bool   m_buy;
};

class CTradePlan{
public:
	string m_sTradePath;

	enum EPlanUpdateID {
		eTradeNoStatus, eTradeStart, eTradeCancel, eTradeComplete
	};

	class CTradePlanNode{
	public:
		string m_sTradePathName;
		string m_sMarketName;
		double m_dPrice{ 0.0 };
		double m_dSize{ 0.0 };
		bool m_bBuy{ true };

		bool m_bFilled{ false };

		EPlanUpdateID eStatus{ eTradeNoStatus };
	};
	vector<CTradePlanNode> m_vPlan;
	size_t m_nFillCheckCounter{ 0 };

	void Initialize(){
		m_vPlan.clear();
		m_nFillCheckCounter = 0;
	}
	bool IsAllFilled();
	double OptimizeTradeSize();
	void SetPathName2Node();
};

class CTradePlans : public vector<CTradePlan>{
protected:
	deque<pair<size_t, size_t>> m_pairFoundNode;

	bool FindPlan( const string& sMarketName );
public:
	void UpdateOrderBook( const string& sMarketName, CAbsOrderBook & orderBook );
};
