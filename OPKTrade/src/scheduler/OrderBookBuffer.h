#pragma once

#include "abstract/AbsOrderBook.h"

class COrderBookBuffer {
private:
	mutex m_mtxOrderBook;
	vector<deque<CAbsOrderBook>> m_orderBookBuffer;
	map<string, size_t> m_mapMarketNameTable;

public:
	void InitializeOrderBookQueue();
	map<string, size_t>& GetMarketNameTable();

	deque<CAbsOrderBook> GetOrderBookBufferQueue( const string& sMarketName );
	int WriteOrderBook( const string& sMarketName, CAbsOrderBook& orderBook );
	void ClearOrderBook( const string &sMarketName );
};