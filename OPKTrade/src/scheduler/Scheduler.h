#pragma once

#include "OrderBookBuffer.h"
#include "SummaryBuffer.h"
#include "ReceiverBuffer.h"

class CScheduleManager {

public:
	void TradeCallback(const string &uuid);
	void OrderStatusTradeCallback(const string &uuid);

	static shared_ptr<CScheduleManager> GetInstance();
private:
	static shared_ptr<CScheduleManager> m_instance;
};