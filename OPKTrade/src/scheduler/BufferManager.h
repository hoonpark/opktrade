#pragma once

class COrderBookBuffer;
class CSummaryBuffer;
class CReceiverBuffer;
class CBufferManager
{
public:
	CBufferManager();

	shared_ptr<COrderBookBuffer> GetOrderBookBuffer();
	shared_ptr<CSummaryBuffer> GetSummaryBuffer();
	shared_ptr<CReceiverBuffer> GetReceiverBuffer();
	static shared_ptr<CBufferManager> GetInstance();

private:
	shared_ptr<COrderBookBuffer> m_orderBookBuffer;
	shared_ptr<CSummaryBuffer> m_summaryBuffer;
	shared_ptr<CReceiverBuffer> m_receiverBuffer;
	static shared_ptr<CBufferManager> m_instance;
};