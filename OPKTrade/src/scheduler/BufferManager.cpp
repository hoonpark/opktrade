#include "stdafx.h"
#include "BufferManager.h"
#include "OrderBookBuffer.h"
#include "SummaryBuffer.h"
#include "ReceiverBuffer.h"

shared_ptr<CBufferManager> CBufferManager::m_instance;

CBufferManager::CBufferManager()
{
	m_orderBookBuffer = make_shared<COrderBookBuffer>();
	m_summaryBuffer = make_shared<CSummaryBuffer>();
	m_receiverBuffer = make_shared<CReceiverBuffer>();

}

shared_ptr<COrderBookBuffer> CBufferManager::GetOrderBookBuffer()
{
	return m_orderBookBuffer;
}

shared_ptr<CBufferManager> CBufferManager::GetInstance()
{
	if (nullptr == m_instance) 
		m_instance = make_shared<CBufferManager>();
	return m_instance;
}

shared_ptr<CSummaryBuffer> CBufferManager::GetSummaryBuffer()
{
	return m_summaryBuffer;
}

shared_ptr<CReceiverBuffer> CBufferManager::GetReceiverBuffer()
{
	return m_receiverBuffer;
}