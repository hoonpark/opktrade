#include "stdafx.h"

#include "Scheduler.h"


shared_ptr<CScheduleManager> CScheduleManager::m_instance;

void CScheduleManager::TradeCallback(const string &uuid)
{
}

void CScheduleManager::OrderStatusTradeCallback(const string &uuid)
{

}

shared_ptr<CScheduleManager> CScheduleManager::GetInstance()
{
	if (!m_instance)
		m_instance = make_shared<CScheduleManager>();

	return m_instance;
}