#include "stdafx.h"
#include "ExchangeManager.h"
#include "../receiver/BittrexReceiver.h"
#include "../receiver/BinanceReceiver.h"
#include "../sender/BittrexSender.h"
#include "../sender/BinanceSender.h"


CPairExchange::CPairExchange()
{
}

int CPairExchange::GetMarketList(CAbsMarketList& absMarketList)
{
	return m_receiver->GetMarketList(absMarketList);
}

int CPairExchange::GetMarketSummary(CAbsMarketList& absMarketList)
{
	return m_receiver->GetMarketSummary(absMarketList);
}

CPairBittrexExchange::CPairBittrexExchange()
{
	m_receiver = make_shared<CBittrexReceiver>();
	m_sender = make_shared<CBittrexSender>();
}

CPairBinanceExchange::CPairBinanceExchange()
{
	m_receiver = make_shared<CBinanceReceiver>();
	m_sender = make_shared<CBinanceSender>();
}

CExchangeFactory::CExchangeFactory()
{
}

shared_ptr<CPairExchange> CExchangeFactory::CreateInstance(const string exchange)
{
	if ("bittrex" == exchange)
		return make_shared<CPairBittrexExchange>();
	else if ("binance" == exchange)
		return make_shared<CPairBinanceExchange>();

	return nullptr;
}