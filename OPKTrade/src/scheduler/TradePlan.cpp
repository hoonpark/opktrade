#include "stdafx.h"

#include "TradePlan.h"
#include "abstract/AbsOrderBook.h"

bool CTradePlans::FindPlan( const string & sMarketName )
{
	m_pairFoundNode.clear();
	for( size_t i = 0; i < size(); ++i ){
		size_t j = 0;
		for( auto& node : at( i ).m_vPlan ){
			if( node.m_sMarketName == sMarketName ){
				m_pairFoundNode.push_back( make_pair( i, j ) );
				//m_nFoundPlan = i;
				//m_nFoundNode = j;
				return true;
			}
			j++;
		}
	}

	return false;
}

void CTradePlans::UpdateOrderBook( const string & sMarketName, CAbsOrderBook & orderBook )
{
	bool bOrderLog = false;
	if( FindPlan( sMarketName ) ){
		while( m_pairFoundNode.size() > 0 ){
			auto foundIndex = m_pairFoundNode.front();
			auto& plan = at( foundIndex.first );
			auto& node = plan.m_vPlan[foundIndex.second];
			m_pairFoundNode.pop_front();

			const int nOrderReportLimit = 2;

			if( orderBook.buy.size() > 0 && node.m_bBuy == false ){
				node.m_dPrice = (double) orderBook.buy[0].m_price;
				node.m_dSize = (double) orderBook.buy[0].m_size;
				node.m_bBuy = false;

				if( bOrderLog )
				{
					for( int j = 0; j < orderBook.buy.size(); j++ )
					{
						ostringstream ss;
						if( j > nOrderReportLimit ) break;
						ss << std::fixed << std::setprecision( 8 ) << "marknet: " << sMarketName << ", buy quantity: " << orderBook.buy.at( j ).m_size << ", buy rate: " << orderBook.buy.at( j ).m_price << endl;
						DebugLog.Write( ss.str() );
					}
				}
				node.m_bFilled = true;

			} else if( orderBook.sell.size() > 0 && node.m_bBuy == true ){
				node.m_dPrice = (double) orderBook.sell[0].m_price;
				node.m_dSize = (double) orderBook.sell[0].m_size;
				node.m_bBuy = true;

				if( bOrderLog )
				{
					for( int j = 0; j < orderBook.sell.size(); j++ )
					{
						ostringstream ss;
						if( j > nOrderReportLimit ) break;
						ss << std::fixed << std::setprecision( 8 ) << "marknet: " << sMarketName << ", sell quantity: " << orderBook.sell.at( j ).m_size << ", sell rate: " << orderBook.sell.at( j ).m_price << endl;
						DebugLog.Write( ss.str() );
					}
				}
				node.m_bFilled = true;

			}

			if( plan.IsAllFilled() ){
				if( plan.OptimizeTradeSize() > 0 ){
					CBittrexReceiverBuffer::WriteTradeRequest( plan.m_vPlan );
					CBittrexReceiverBuffer::InsertTradingStatus( plan );
				}

				at( foundIndex.first ) = at( size() - 1 );
				resize( size() - 1 );

				/*
				for( auto& node : plan.m_vPlan ){
					CBittrexReceiverBuffer::WriteTradeRequest( node.m_sMarketName, node.m_dPrice, node.m_dSize, node.m_bBuy );
				}
				*/
			}
		}
	}
}

bool CTradePlan::IsAllFilled()
{
	bool bResult = true;

	for( auto& node : m_vPlan ){
		bResult &= node.m_bFilled;
	}
	if( bResult ){
		for( auto& node : m_vPlan ){
			node.m_bFilled = false;
		}
	}

	return bResult;
}

double CTradePlan::OptimizeTradeSize()
{
	stringstream sLog;
	double sTradeRate = 0;
	double dBuyComm = 0.0025;
	double dSellComm = 0.0025;

	enum ECapType {
		eETHCap, eBTCCap, eUSDTCap
	};
	const string sETH{ "ETH" };
	const string sBTC{ "BTC" };
	const string sUSDT{ "USDT" };
	auto GetStartMarket = []( const string& sMarketName )->ECapType {
		if( sMarketName.find( "ETH" ) != string::npos )	return eETHCap;
		if( sMarketName.find( "BTC" ) != string::npos )	return eBTCCap;
		return eUSDTCap;
	};
	auto GetCapValue = []( const ECapType nCapType ) -> double {
		switch( nCapType ){
		case eETHCap:		return 0.1;
		case eBTCCap:		return 0.01;
		default:			return 50.0;
		}
	};
	auto GetMinCapValue = []( const ECapType nCapType ) -> double {
		switch( nCapType ){
		case eETHCap:		return 0.0005;
		case eBTCCap:		return 0.0005;
		default:			return 0.05;
		}
	};
	auto GetBaseMarketName = []( const ECapType nCapType ) -> string {
		switch( nCapType ){
		case eETHCap:		return "ETH";
		case eBTCCap:		return "BTC";
		default:			return "USDT";
		}
	};

	ECapType nCapType = GetStartMarket( m_vPlan[0].m_sMarketName );
	ECapType nCapType2 = GetStartMarket( m_vPlan[1].m_sMarketName );
	double dCap = GetCapValue( nCapType );
	double dMinCap = GetMinCapValue( nCapType ); 

	SetPathName2Node();
	sLog << m_sTradePath << " Optimizing Processing..." << endl;

	string sOptimizingLogPrev[3];
	size_t nIndex = 0;
	for( auto& plan : m_vPlan ){
		stringstream sLogPrev;
		sLogPrev.str( "" );
		sLogPrev << setw( 16 ) << setprecision( 8 ) << fixed << plan.m_dSize;
		sOptimizingLogPrev[nIndex++] = sLogPrev.str();
	}

	if( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) > dCap ){
		m_vPlan[0].m_dSize = dCap / m_vPlan[0].m_dPrice;
	}

	// Size Optimizing
	if( m_vPlan[0].m_dSize > m_vPlan[1].m_dSize ){
		m_vPlan[0].m_dSize = m_vPlan[1].m_dSize;
	} else if( m_vPlan[0].m_dSize < m_vPlan[1].m_dSize ){
		m_vPlan[1].m_dSize = m_vPlan[0].m_dSize;
	}

	if( m_vPlan[2].m_bBuy ){
		if( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) < ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) ){
			double dNewSize = ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) / m_vPlan[1].m_dPrice;
			m_vPlan[0].m_dSize = m_vPlan[1].m_dSize = dNewSize;
		} else {
			m_vPlan[2].m_dSize = ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) / m_vPlan[2].m_dPrice;
		}
		sTradeRate = ( m_vPlan[2].m_dSize ) - ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice )
			- ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) * dBuyComm )
			- ( ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) * dSellComm ) / m_vPlan[2].m_dPrice )
			- ( ( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) * dBuyComm ) / m_vPlan[2].m_dPrice )
			;
	} else {
		if( ( m_vPlan[2].m_dSize ) < ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) ){
			double dNewSize = ( m_vPlan[2].m_dSize ) / m_vPlan[1].m_dPrice;
			m_vPlan[0].m_dSize = m_vPlan[1].m_dSize = dNewSize;
		} else {
			m_vPlan[2].m_dSize = ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice );
		}
		sTradeRate = ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) - ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice )
			- ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) * dBuyComm )
			- ( ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) * dSellComm ) * m_vPlan[2].m_dPrice )
			- ( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) * dBuyComm )
			;
	}

	nIndex = 0;
	for( auto& plan : m_vPlan ){
		sLog << " [" << plan.m_sMarketName << "] : " << ( plan.m_bBuy ? "BUY" : "SELL" ) << " : "
			<< sOptimizingLogPrev[nIndex++] << " --> " << setw( 16 ) << setprecision( 8 ) << fixed << plan.m_dSize << " (" << plan.m_dPrice << ")" << endl;
	}
	sLog << "-Respect Rate : " << sTradeRate << " " << m_vPlan[0].m_sMarketName.substr( 0, 3 ) << endl;

	stringstream sLogMinimum;
	bool bMinimumNotMet = true;
	double dMinimumPrice = 0;
	ECapType nMinimumType = eETHCap;
	
	if( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) < GetMinCapValue( nCapType ) ){
		dMinimumPrice = m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice;
		nMinimumType = nCapType;
		sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[0].m_dSize << " (" << m_vPlan[0].m_dPrice << ") ";
	} else if(  ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) < GetMinCapValue( nCapType2 ) ) {
		dMinimumPrice = m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice;
		nMinimumType = nCapType2;
		sLogMinimum << "2nd Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[1].m_dSize << " (" << m_vPlan[1].m_dPrice << ") ";
	//} else if( nCapType == eETHCap && ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) < dMinCap ) ){
	//	dMinimumPrice = m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice;
	//	nMinimumType = eETHCap;
	//	sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[0].m_dSize << " (" << m_vPlan[0].m_dPrice << ") ";
	//} else if( nCapType2 == eETHCap && ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) < GetMinCapValue( nCapType2 ) ) ){
	//	dMinimumPrice = m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice;
	//	nMinimumType = eETHCap;
	//	sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[1].m_dSize << " (" << m_vPlan[1].m_dPrice << ") ";
	} else {
		bMinimumNotMet = false;
	}
	if( bMinimumNotMet ){
		sLog.str( "" );
		sLog << m_sTradePath << " Optimizing Processing... ";
		sLog << "Min-Trade Detected : " << sLogMinimum.str() << "- " << dMinimumPrice << ( nMinimumType == eBTCCap ? " BTC" : " ETH" ) << endl;
		DebugLog.Write( sLog.str() );
		return -1.0;
	}


	DebugLog.Write( sLog.str() );

	return sTradeRate;
}

void CTradePlan::SetPathName2Node()
{
	for( auto& node : m_vPlan ){
		node.m_sTradePathName = m_sTradePath;
	}
}
