#include "stdafx.h"
#include "SummaryBuffer.h"

bool CSummaryBuffer::WriteSummaries( vector<CAbsMarketSummary> &summaries )
{
	lock_guard<mutex> sync( m_mtxMarketSummary );

	m_marketSummariesBuffer.clear();
	m_marketSummariesBuffer = summaries;	// copy & push
	return true;
}

vector<CAbsMarketSummary>& CSummaryBuffer::GetSummaries()
{
	static vector<CAbsMarketSummary> summaries;

	lock_guard<mutex> sync( m_mtxMarketSummary );
	summaries.clear();
	summaries = m_marketSummariesBuffer;
	return summaries;
}

void CSummaryBuffer::ClearSummaries()
{
	lock_guard<mutex> sync( m_mtxMarketSummary );
	m_marketSummariesBuffer.clear();
}
