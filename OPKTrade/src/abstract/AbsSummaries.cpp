#include "stdafx.h"
#include "AbsSummaries.h"


CAbsMarketSummary::CAbsMarketSummary()
{

}

CAbsMarketSummary::CAbsMarketSummary(const json &j_market_sum)
{
	market_name = j_market_sum["MarketName"];
	high = j_market_sum["High"].get_double();
	low = j_market_sum["Low"].get_double();
	volume = j_market_sum["Volume"].get_double();
	last = j_market_sum["Last"].get_double();
	base_volume = j_market_sum["BaseVolume"].get_double();
	time_stamp = j_market_sum["TimeStamp"];
	bid = j_market_sum["Bid"].get_double();
	ask = j_market_sum["Ask"].get_double();
	open_buy_orders = j_market_sum["OpenBuyOrders"].get_int();
	open_sell_orders = j_market_sum["OpenSellOrders"].get_int();
	prev_day = j_market_sum["PrevDay"];
	created = j_market_sum["Created"];
}