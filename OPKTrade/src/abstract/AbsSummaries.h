#pragma once

#include "../receiver/lib/json.hpp"

using json = nlohmann::json;
class CAbsMarketSummary
{
public:
	CAbsMarketSummary();
	explicit CAbsMarketSummary(const json &j_market_sum);

public:
	string market_name;
	double high;
	double low;
	double volume;
	double last;
	double base_volume;
	string time_stamp;
	double bid;
	double ask;
	int open_buy_orders;
	int open_sell_orders;
	double prev_day;
	string created;
};

