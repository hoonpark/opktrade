#pragma once

#include "receiver/lib/json.hpp"

using json = nlohmann::json;

class CAbsOrderBookEntry {
public:
	explicit CAbsOrderBookEntry( json j ) {
		m_size = j["Quantity"];
		m_price = j["Rate"];
	};

	CAbsOrderBookEntry(const double &price, const double &size)
	{
		m_price = price;
		m_size = size;
	};

	double m_size;
	double m_price;
};


class CAbsOrderBook {
public:
	CAbsOrderBook( json o_book, const std::string &type );
	CAbsOrderBook() = default;

	vector<CAbsOrderBookEntry> buy;
	vector<CAbsOrderBookEntry> sell;
};
