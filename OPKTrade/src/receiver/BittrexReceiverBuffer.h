#pragma once

#include "bittrex/exchange/bittrex.h"
#include "bittrex/lib/utils.h"
#include "bittrex/api/public.h"
#include "abstract/AbsSummaries.h"

class TradeRequest
{
public:
	TradeRequest(string marketName, double quantity, double rate, bool buy) : m_marketName(marketName), m_quantity(quantity), m_rate(rate), m_buy(buy)
	{

	};
public:
	string m_marketName;
	double m_quantity;
	double m_rate;
	bool   m_buy;
};

class CTradePlan{
public:
	string m_sTradePath;

	enum EPlanUpdateID {
		eTradeNoStatus, eTradeStart, eTradeCancel, eTradeComplete
	};

	class CTradePlanNode{
	public:
		string m_sTradePathName;
		string m_sMarketName;
		double m_dPrice{ 0.0 };
		double m_dSize{ 0.0 };
		bool m_bBuy{ true };

		bool m_bFilled{ false };

		EPlanUpdateID eStatus{ eTradeNoStatus };
	};
	vector<CTradePlanNode> m_vPlan;
	size_t m_nFillCheckCounter{ 0 };

	void Initialize(){
		m_vPlan.clear();
		m_nFillCheckCounter = 0;
	}
	bool IsAllFilled();
	double OptimizeTradeSize();
	void SetPathName2Node();
};

class CTradePlans : public vector<CTradePlan>{
protected:
	deque<pair<size_t, size_t>> m_pairFoundNode;
	//size_t m_nFoundNode;
	//size_t m_nFoundPlan;

	bool FindPlan( const string& sMarketName );
public:
	void UpdateOrderBook( const string& sMarketName, CAbsOrderBook & orderBook );
};

class CBittrexReceiverBuffer {
private:
	// Thread 에서 사용할 Order Book Buffer의 vector와 mutex
	//		0~2	: BTC-ETH, USDT-BTC, USDT-ETH market
	//		3~	: 그 외 Thread에서 담당할 Order Book 들
	static mutex m_mtxOrderBook;
	static vector<deque<CAbsOrderBook>> m_orderBookBuffer;
	static map<string, size_t> m_mapMarketNameTable;

	static mutex m_mtxOrderRequest;
	static deque<pair<string, bool>> m_qOrderRequest;

	static mutex m_mtxTradeRequest;
	static deque<vector<CTradePlan::CTradePlanNode>> m_qTradeRequest;
	static deque<string> m_qTradeRequestForOpenedOrder;

	static mutex m_mtxOpendOrder;
	static deque<string> m_openedOrder;

	static CTradePlans m_TradePlan;

	static mutex m_mtxOnTrading;
	static vector<CTradePlan> m_vOnTrading;

public:
	static vector<string> m_orderBookNameBuffer;

private:
	// Thread 에서 사용할 Summary Buffer와 mutex
	static mutex m_mtxMarketSummary;
	static List<response::MarketSummary> m_marketSummariesBuffer;

public:
	CBittrexReceiverBuffer();
	static void InitializeOrderBookQueue();

public:
	static map<string, size_t>& GetMarketNameTable();

	static void WriteOpenedOrder(const string &uuid);
	static deque<string> GetOpenedOrder();
	static void ClearOpenedOrder();

	static deque<string> GetTradeRequestForOpenedOrder();
	static bool GetOpenedOrderName(string &marketName);
	static void DeleteFrontOpenedOrderName();

	static void WriteTradeRequest(const vector<CTradePlan::CTradePlanNode> &vPlan);
	static void WriteTradeRequest(const string marketName, const double quantity, const double rate, const bool buy);
	static vector<CTradePlan::CTradePlanNode>& GetTradeRequest();

	static deque<CAbsOrderBook> GetOrderBookBufferQueue( const string& sMarketName );
	static int WriteOrderBook( const string& sMarketName, CAbsOrderBook& orderBook );
	static void ClearOrderBook(const string &sMarketName );

	static void WriteOrderRequest( const string& sMarketIndex, const bool bBuy );
	static bool GetOrderRequest( string& nMarketIndex, bool& bBuy );

	static bool WriteSummaries(List<response::MarketSummary> &summaries);
	static List<response::MarketSummary>& GetSummaries();
	static void ClearSummaries();

	static void SetTradePlan( CTradePlan tradePlan );
	static bool IsExistPlan( string& sPlanPathName );
	static bool UpdateTradePlan( const string& sMarketName, CAbsOrderBook& orderBook );

	// Trading Status Checking
	static void InsertTradingStatus( CTradePlan& plan );
	static void UpdateTradingStatus( CTradePlan::CTradePlanNode& planNode, CTradePlan::EPlanUpdateID nUpdateID );
	static bool CheckOnTrading( const string& sPlanPathName );

public:
	static string ExtractOrderBook( CAbsOrderBook& orderBook );
};
