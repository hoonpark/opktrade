#pragma once

#include "abstract/AbsMarket.h"
#include "abstract/AbsOrderBook.h"
#include "scheduler/OrderBookBuffer.h"

class CThreadOperatorBase;
class CReceiver
{
protected:
	COrderBookBuffer m_OrderBookBuffer;

	shared_ptr<CThreadOperatorBase> m_threadSummaries;
	shared_ptr<CThreadOperatorBase> m_threadAccountStatus;
	vector<shared_ptr<CThreadOperatorBase>> m_vecOrderStatus;
	map<string, shared_ptr<CThreadOperatorBase>> m_vecThreadOnlyOrderBookUpdater;

public:
	CReceiver() = default;
	virtual ~CReceiver() = default;

	virtual void Run() = 0;
	virtual void Stop() = 0;

	virtual int GetMarketList( CAbsMarketList& absMarketList ) = 0;
	virtual int GetMarketSummary( CAbsMarketList& absMarketList ) = 0;

	virtual double GetMarketPrice( const string& sMarketName ) = 0;
};
