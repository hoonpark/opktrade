#include "stdafx.h"

#include "BittrexReceiverBuffer.h"

mutex CBittrexReceiverBuffer::m_mtxOrderBook;
mutex CBittrexReceiverBuffer::m_mtxOrderRequest;
mutex CBittrexReceiverBuffer::m_mtxMarketSummary;
mutex CBittrexReceiverBuffer::m_mtxTradeRequest;
mutex CBittrexReceiverBuffer::m_mtxOpendOrder;

List<response::MarketSummary> CBittrexReceiverBuffer::m_marketSummariesBuffer;
vector<deque<CAbsOrderBook>> CBittrexReceiverBuffer::m_orderBookBuffer;
vector<string> CBittrexReceiverBuffer::m_orderBookNameBuffer;


deque<pair<string, bool>> CBittrexReceiverBuffer::m_qOrderRequest;

map<string, size_t> CBittrexReceiverBuffer::m_mapMarketNameTable;

deque<vector<CTradePlan::CTradePlanNode>> CBittrexReceiverBuffer::m_qTradeRequest;
deque<string> CBittrexReceiverBuffer::m_qTradeRequestForOpenedOrder;
deque<string> CBittrexReceiverBuffer::m_openedOrder;

CTradePlans CBittrexReceiverBuffer::m_TradePlan;

mutex CBittrexReceiverBuffer::m_mtxOnTrading;
vector<CTradePlan> CBittrexReceiverBuffer::m_vOnTrading;


void CBittrexReceiverBuffer::InitializeOrderBookQueue()
{
	// TODO: Initialize 관리 수정해야함
	//m_mapMarketNameTable["BTC-ETH"] = 0;
	//m_mapMarketNameTable["USDT-BTC"] = 1;
	//m_mapMarketNameTable["USDT-ETH"] = 2;
	//m_mapMarketNameTable["BTC-XRP"] = 3;
	//m_mapMarketNameTable["ETH-XRP"] = 4;
	//m_mapMarketNameTable["USDT-XRP"] = 5;
	//m_mapMarketNameTable["BTC-NEO"] = 6;
	//m_mapMarketNameTable["ETH-NEO"] = 7;
	//m_mapMarketNameTable["USDT-NEO"] = 8;

	m_orderBookBuffer.resize( m_mapMarketNameTable.size() );
}


CBittrexReceiverBuffer::CBittrexReceiverBuffer()
{
}


map<string, size_t>& CBittrexReceiverBuffer::GetMarketNameTable()
{
	return m_mapMarketNameTable;
}

deque<CAbsOrderBook> CBittrexReceiverBuffer::GetOrderBookBufferQueue( const string & sMarketName )
{
	static deque<CAbsOrderBook> queueResult;

	lock_guard<mutex> sync( m_mtxOrderBook );

	// TODO: 2중 deep copy 일어나지 않도록 개선할 것
	queueResult.clear();
	queueResult = m_orderBookBuffer[m_mapMarketNameTable[sMarketName]];		// deep copy
	m_orderBookBuffer[m_mapMarketNameTable[sMarketName]].clear();

	return queueResult;		// deep copy
}

int CBittrexReceiverBuffer::WriteOrderBook( const string & sMarketName, CAbsOrderBook & orderBook )
{
	lock_guard<mutex> sync( m_mtxOrderBook );

	if( m_mapMarketNameTable.find( sMarketName ) != m_mapMarketNameTable.end() ){
		m_orderBookBuffer[m_mapMarketNameTable[sMarketName]].push_back( orderBook );	// copy & push
		return (int) m_mapMarketNameTable[sMarketName];
	}

	return -1;
}

void CBittrexReceiverBuffer::ClearOrderBook(const string &sMarketName)
{
	lock_guard<mutex> sync(m_mtxOrderBook);

	if (m_mapMarketNameTable.find(sMarketName) != m_mapMarketNameTable.end()) {
		m_orderBookBuffer[m_mapMarketNameTable[sMarketName]].clear();
	}
}

void CBittrexReceiverBuffer::WriteOrderRequest( const string& sMarketIndex, const bool bBuy )
{
	lock_guard<mutex> sync( m_mtxOrderRequest );
	m_qOrderRequest.emplace_back( make_pair( sMarketIndex, bBuy ) );
}

bool CBittrexReceiverBuffer::GetOrderRequest( string & nMarketIndex, bool & bBuy )
{
	lock_guard<mutex> sync( m_mtxOrderRequest );

	if( m_qOrderRequest.empty() ) return false;

	auto& order = m_qOrderRequest.front();

	nMarketIndex = order.first;
	bBuy = order.second;

	m_qOrderRequest.pop_front();

	return true;
}

bool CBittrexReceiverBuffer::WriteSummaries(List<response::MarketSummary> &summaries)
{
	lock_guard<mutex> sync( m_mtxMarketSummary );

	m_marketSummariesBuffer.clear();
	m_marketSummariesBuffer = summaries;	// copy & push
	return true;
}

List<response::MarketSummary>& CBittrexReceiverBuffer::GetSummaries()
{
	static List<response::MarketSummary> summaries;

	lock_guard<mutex> sync(m_mtxMarketSummary);
	summaries.clear();
	summaries = m_marketSummariesBuffer;
	return summaries;
}

void CBittrexReceiverBuffer::ClearSummaries()
{
	lock_guard<mutex> sync(m_mtxMarketSummary);
	m_marketSummariesBuffer.clear();
}

void CBittrexReceiverBuffer::SetTradePlan( CTradePlan tradePlan )
{
	for( auto iter = m_TradePlan.begin(); iter != m_TradePlan.end(); ++ iter ){
		if( iter->m_sTradePath == tradePlan.m_sTradePath ){
			*iter = tradePlan;
			//m_TradePlan.erase( iter );
			return;
		}
	}

	m_TradePlan.emplace_back( tradePlan );
}

bool CBittrexReceiverBuffer::IsExistPlan( string & sPlanPathName )
{
	const size_t cnCounterLimit{ 10 };

	for( auto& plan : m_TradePlan ){
		if( plan.m_sTradePath == sPlanPathName ){
			plan.m_nFillCheckCounter++;
			if( plan.m_nFillCheckCounter > cnCounterLimit ){
				for( auto& node : plan.m_vPlan ){
					if( !node.m_bFilled ){
						CBittrexReceiverBuffer::WriteOrderRequest( node.m_sMarketName, node.m_bBuy );
					}
				}
				plan.m_nFillCheckCounter = 0;
			}

			return true;
		}
	}

	return false;
}

bool CBittrexReceiverBuffer::UpdateTradePlan( const string& sMarketName, CAbsOrderBook & orderBook )
{
	m_TradePlan.UpdateOrderBook( sMarketName, orderBook );

	//WriteTradeRequest;
	return false;
}


vector<CTradePlan::CTradePlanNode>& CBittrexReceiverBuffer::GetTradeRequest()
{
	static vector<CTradePlan::CTradePlanNode> vec;
	lock_guard<mutex> sync(m_mtxTradeRequest);
	
	vec.clear();

	if (m_qTradeRequest.empty()) return vec;

	auto& trade = m_qTradeRequest.front();
	vec.swap(trade);

	m_qTradeRequest.pop_front();

	return vec;

}

deque<string> CBittrexReceiverBuffer::GetOpenedOrder()
{
	static deque<string> uuids;
	lock_guard<mutex> sync(m_mtxOpendOrder);
	
	uuids = m_openedOrder;

	return uuids;
}

void CBittrexReceiverBuffer::ClearOpenedOrder()
{
	lock_guard<mutex> sync(m_mtxOpendOrder);
	m_openedOrder.clear();
}

void CBittrexReceiverBuffer::WriteOpenedOrder(const string &uuid)
{
	lock_guard<mutex> sync(m_mtxOpendOrder);
	if (std::find(m_openedOrder.begin(), m_openedOrder.end(), uuid) == m_openedOrder.end())
		m_openedOrder.emplace_back(uuid);
}


void CBittrexReceiverBuffer::DeleteFrontOpenedOrderName()
{
	lock_guard<mutex> sync(m_mtxTradeRequest);

	m_qTradeRequestForOpenedOrder.pop_front();
}

bool CBittrexReceiverBuffer::GetOpenedOrderName(string &marketName)
{
	lock_guard<mutex> sync(m_mtxTradeRequest);

	if (m_qTradeRequestForOpenedOrder.empty()) return false;

	marketName = m_qTradeRequestForOpenedOrder.front();

	return true;
}

deque<string> CBittrexReceiverBuffer::GetTradeRequestForOpenedOrder()
{
	static deque<string> openedOrder;

	lock_guard<mutex> sync(m_mtxTradeRequest);

	openedOrder = m_qTradeRequestForOpenedOrder;
	return openedOrder;
}

void CBittrexReceiverBuffer::WriteTradeRequest(const vector<CTradePlan::CTradePlanNode> &vPlan)
{
	lock_guard<mutex> sync(m_mtxTradeRequest);
	vector<CTradePlan::CTradePlanNode> v;
	v.resize(vPlan.size());
	v = vPlan;
	m_qTradeRequest.emplace_back(v);

	for (auto node : vPlan)
	{
		WriteTradeRequest(node.m_sMarketName, node.m_dSize, node.m_dPrice, node.m_bBuy);
	}
}
void CBittrexReceiverBuffer::WriteTradeRequest(const string marketName, const double quantity, const double rate, const bool buy)
{
	//lock_guard<mutex> sync(m_mtxTradeRequest);

	//m_qTradeRequest.emplace_back( TradeRequest(marketName, quantity, rate, buy) );
	if (std::find(m_qTradeRequestForOpenedOrder.begin(), m_qTradeRequestForOpenedOrder.end(), marketName) == m_qTradeRequestForOpenedOrder.end())
		m_qTradeRequestForOpenedOrder.emplace_back(marketName);
}

void CBittrexReceiverBuffer::InsertTradingStatus( CTradePlan & plan )
{
	lock_guard<mutex> sync( m_mtxOnTrading );

	m_vOnTrading.push_back( plan );
}

void CBittrexReceiverBuffer::UpdateTradingStatus( CTradePlan::CTradePlanNode & planNode, CTradePlan::EPlanUpdateID nUpdateID )
{
	lock_guard<mutex> sync( m_mtxOnTrading );

	for( auto iter = m_vOnTrading.begin(); iter != m_vOnTrading.end(); ++iter ){
		if( iter->m_sTradePath == planNode.m_sTradePathName ){
			bool bStatusCheck = false;
			int nCompleteCount = 0;

			for( auto& node : iter->m_vPlan ){
				if( node.m_sMarketName == planNode.m_sMarketName ){
					node.eStatus = nUpdateID;
				}

				if( node.eStatus == CTradePlan::eTradeCancel ){
					bStatusCheck = true;
					break;
				} else if( node.eStatus == CTradePlan::eTradeComplete ){
					nCompleteCount++;
				} else {
					bStatusCheck = false;
				}
			}
			if( nCompleteCount == iter->m_vPlan.size() )
				bStatusCheck = true;

			if( bStatusCheck ){
				m_vOnTrading.erase( iter );
			}

			break;
		}
	}
}

bool CBittrexReceiverBuffer::CheckOnTrading( const string & sPlanPathName )
{
	lock_guard<mutex> sync( m_mtxOnTrading );

	for( auto& plan : m_vOnTrading ){
		if( plan.m_sTradePath == sPlanPathName ){
			return true;
		}
	}

	return false;
}

string CBittrexReceiverBuffer::ExtractOrderBook( CAbsOrderBook & orderBook )
{
	string result;

	{
		const int nOrderReportLimit = 2;

		for( int i = 0; i < orderBook.sell.size(); i++ )
		{
			if( i > nOrderReportLimit ) break;
			CAbsOrderBookEntry entry = orderBook.sell.at( i );
			result += "sell: " + to_string( i + 1 ) + ": " + to_string( entry.size ) + ", " + to_string( entry.price ) + "\n";
		}

		for( int i = 0; i < orderBook.buy.size(); i++ )
		{
			if( i > nOrderReportLimit ) break;
			CAbsOrderBookEntry entry = orderBook.buy.at( i );
			result += "buy: " + to_string( i + 1 ) + ": " + to_string( entry.size ) + ", " + to_string( entry.price ) + "\n";
		}
	}

	return result;
}

bool CTradePlans::FindPlan( const string & sMarketName )
{
	m_pairFoundNode.clear();
	for( size_t i = 0; i < size(); ++i ){
		size_t j = 0;
		for( auto& node : at( i ).m_vPlan ){
			if( node.m_sMarketName == sMarketName ){
				m_pairFoundNode.push_back( make_pair( i, j ) );
				//m_nFoundPlan = i;
				//m_nFoundNode = j;
				return true;
			}
			j++;
		}
	}

	return false;
}
#include "../../Debug.h"
void CTradePlans::UpdateOrderBook( const string & sMarketName, CAbsOrderBook & orderBook )
{
	bool bOrderLog = false;
	if( FindPlan( sMarketName ) ){
		while( m_pairFoundNode.size() > 0 ){
			auto foundIndex = m_pairFoundNode.front();
			auto& plan = at( foundIndex.first );
			auto& node = plan.m_vPlan[foundIndex.second];
			m_pairFoundNode.pop_front();

			const int nOrderReportLimit = 2;

			if( orderBook.buy.size() > 0 && node.m_bBuy == false ){
				node.m_dPrice = (double) orderBook.buy[0].price;
				node.m_dSize = (double) orderBook.buy[0].size;
				node.m_bBuy = false;

				if( bOrderLog )
				{
					for( int j = 0; j < orderBook.buy.size(); j++ )
					{
						ostringstream ss;
						if( j > nOrderReportLimit ) break;
						ss << std::fixed << std::setprecision( 8 ) << "marknet: " << sMarketName << ", buy quantity: " << orderBook.buy.at( j ).size << ", buy rate: " << orderBook.buy.at( j ).price << endl;
						DebugLog.Write( ss.str() );
					}
				}
				node.m_bFilled = true;

			} else if( orderBook.sell.size() > 0 && node.m_bBuy == true ){
				node.m_dPrice = (double) orderBook.sell[0].price;
				node.m_dSize = (double) orderBook.sell[0].size;
				node.m_bBuy = true;

				if( bOrderLog )
				{
					for( int j = 0; j < orderBook.sell.size(); j++ )
					{
						ostringstream ss;
						if( j > nOrderReportLimit ) break;
						ss << std::fixed << std::setprecision( 8 ) << "marknet: " << sMarketName << ", sell quantity: " << orderBook.sell.at( j ).size << ", sell rate: " << orderBook.sell.at( j ).price << endl;
						DebugLog.Write( ss.str() );
					}
				}
				node.m_bFilled = true;

			}


			if( plan.IsAllFilled() ){
				if( plan.OptimizeTradeSize() > 0 ){
					CBittrexReceiverBuffer::WriteTradeRequest( plan.m_vPlan );
					CBittrexReceiverBuffer::InsertTradingStatus( plan );
				}

				at( foundIndex.first ) = at( size() - 1 );
				resize( size() - 1 );

				/*
				for( auto& node : plan.m_vPlan ){
					CBittrexReceiverBuffer::WriteTradeRequest( node.m_sMarketName, node.m_dPrice, node.m_dSize, node.m_bBuy );
				}
				*/
			}
		}
	}
}

bool CTradePlan::IsAllFilled()
{
	bool bResult = true;

	for( auto& node : m_vPlan ){
		bResult &= node.m_bFilled;
	}
	if( bResult ){
		for( auto& node : m_vPlan ){
			node.m_bFilled = false;
		}
	}

	return bResult;
}

double CTradePlan::OptimizeTradeSize()
{
	stringstream sLog;
	double sTradeRate = 0;
	double dBuyComm = 0.0025;
	double dSellComm = 0.0025;

	enum ECapType {
		eETHCap, eBTCCap, eUSDTCap
	};
	const string sETH{ "ETH" };
	const string sBTC{ "BTC" };
	const string sUSDT{ "USDT" };
	auto GetStartMarket = []( const string& sMarketName )->ECapType {
		if( sMarketName.find( "ETH" ) != string::npos )	return eETHCap;
		if( sMarketName.find( "BTC" ) != string::npos )	return eBTCCap;
		return eUSDTCap;
	};
	auto GetCapValue = []( const ECapType nCapType ) -> double {
		switch( nCapType ){
		case eETHCap:		return 0.1;
		case eBTCCap:		return 0.01;
		default:			return 50.0;
		}
	};
	auto GetMinCapValue = []( const ECapType nCapType ) -> double {
		switch( nCapType ){
		case eETHCap:		return 0.0005;
		case eBTCCap:		return 0.0005;
		default:			return 0.05;
		}
	};
	auto GetBaseMarketName = []( const ECapType nCapType ) -> string {
		switch( nCapType ){
		case eETHCap:		return "ETH";
		case eBTCCap:		return "BTC";
		default:			return "USDT";
		}
	};

	ECapType nCapType = GetStartMarket( m_vPlan[0].m_sMarketName );
	ECapType nCapType2 = GetStartMarket( m_vPlan[1].m_sMarketName );
	double dCap = GetCapValue( nCapType );
	double dMinCap = GetMinCapValue( nCapType ); 

	SetPathName2Node();
	sLog << m_sTradePath << " Optimizing Processing..." << endl;

	string sOptimizingLogPrev[3];
	size_t nIndex = 0;
	for( auto& plan : m_vPlan ){
		stringstream sLogPrev;
		sLogPrev.str( "" );
		sLogPrev << setw( 16 ) << setprecision( 8 ) << fixed << plan.m_dSize;
		sOptimizingLogPrev[nIndex++] = sLogPrev.str();
	}

	if( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) > dCap ){
		m_vPlan[0].m_dSize = dCap / m_vPlan[0].m_dPrice;
	}

	// Size Optimizing
	if( m_vPlan[0].m_dSize > m_vPlan[1].m_dSize ){
		m_vPlan[0].m_dSize = m_vPlan[1].m_dSize;
	} else if( m_vPlan[0].m_dSize < m_vPlan[1].m_dSize ){
		m_vPlan[1].m_dSize = m_vPlan[0].m_dSize;
	}

	if( m_vPlan[2].m_bBuy ){
		if( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) < ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) ){
			double dNewSize = ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) / m_vPlan[1].m_dPrice;
			m_vPlan[0].m_dSize = m_vPlan[1].m_dSize = dNewSize;
		} else {
			m_vPlan[2].m_dSize = ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) / m_vPlan[2].m_dPrice;
		}
		sTradeRate = ( m_vPlan[2].m_dSize ) - ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice )
			- ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) * dBuyComm )
			- ( ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) * dSellComm ) / m_vPlan[2].m_dPrice )
			- ( ( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) * dBuyComm ) / m_vPlan[2].m_dPrice )
			;
	} else {
		if( ( m_vPlan[2].m_dSize ) < ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) ){
			double dNewSize = ( m_vPlan[2].m_dSize ) / m_vPlan[1].m_dPrice;
			m_vPlan[0].m_dSize = m_vPlan[1].m_dSize = dNewSize;
		} else {
			m_vPlan[2].m_dSize = ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice );
		}
		sTradeRate = ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) - ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice )
			- ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) * dBuyComm )
			- ( ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) * dSellComm ) * m_vPlan[2].m_dPrice )
			- ( ( m_vPlan[2].m_dSize * m_vPlan[2].m_dPrice ) * dBuyComm )
			;
	}

	nIndex = 0;
	for( auto& plan : m_vPlan ){
		sLog << " [" << plan.m_sMarketName << "] : " << ( plan.m_bBuy ? "BUY" : "SELL" ) << " : "
			<< sOptimizingLogPrev[nIndex++] << " --> " << setw( 16 ) << setprecision( 8 ) << fixed << plan.m_dSize << " (" << plan.m_dPrice << ")" << endl;
	}
	sLog << "-Respect Rate : " << sTradeRate << " " << m_vPlan[0].m_sMarketName.substr( 0, 3 ) << endl;

	stringstream sLogMinimum;
	bool bMinimumNotMet = true;
	double dMinimumPrice = 0;
	ECapType nMinimumType = eETHCap;
	
	if( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) < GetMinCapValue( nCapType ) ){
		dMinimumPrice = m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice;
		nMinimumType = nCapType;
		sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[0].m_dSize << " (" << m_vPlan[0].m_dPrice << ") ";
	} else if(  ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) < GetMinCapValue( nCapType2 ) ) {
		dMinimumPrice = m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice;
		nMinimumType = nCapType2;
		sLogMinimum << "2nd Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[1].m_dSize << " (" << m_vPlan[1].m_dPrice << ") ";
	//} else if( nCapType == eETHCap && ( ( m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice ) < dMinCap ) ){
	//	dMinimumPrice = m_vPlan[0].m_dSize * m_vPlan[0].m_dPrice;
	//	nMinimumType = eETHCap;
	//	sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[0].m_dSize << " (" << m_vPlan[0].m_dPrice << ") ";
	//} else if( nCapType2 == eETHCap && ( ( m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice ) < GetMinCapValue( nCapType2 ) ) ){
	//	dMinimumPrice = m_vPlan[1].m_dSize * m_vPlan[1].m_dPrice;
	//	nMinimumType = eETHCap;
	//	sLogMinimum << "1st Trade Plan : " << setw( 16 ) << setprecision( 8 ) << fixed << m_vPlan[1].m_dSize << " (" << m_vPlan[1].m_dPrice << ") ";
	} else {
		bMinimumNotMet = false;
	}
	if( bMinimumNotMet ){
		sLog.str( "" );
		sLog << m_sTradePath << " Optimizing Processing... ";
		sLog << "Min-Trade Detected : " << sLogMinimum.str() << "- " << dMinimumPrice << ( nMinimumType == eBTCCap ? " BTC" : " ETH" ) << endl;
		DebugLog.Write( sLog.str() );
		return -1.0;
	}


	DebugLog.Write( sLog.str() );

	return sTradeRate;
}

void CTradePlan::SetPathName2Node()
{
	for( auto& node : m_vPlan ){
		node.m_sTradePathName = m_sTradePath;
	}
}
