#include "stdafx.h"
#include "BittrexReceiver.h"

#include "scheduler/Scheduler.h"
#include "scheduler/BufferManager.h"
#include "../monitoring/MonitoringDlg.h"
#include "../Debug.h"
#include "../thread/ThreadOperatorBase.h"
#include <time.h>


CBittrexReceiver::CBittrexReceiver() : CReceiver()
{
	m_client = make_shared<bittrex::Client>("f43d4dc22b684975903e9ef5a581d609", "21d222f409164ae896dc6cb83988efb3");
	m_public = make_shared<bittrex::api::Public>(m_client->get_public());

	m_threadSummaries = make_shared<CBittrexMarketSummariesUpdater>("002dbc500bea4b10a5d25b898dbc8ee1", "74796cf7e44842f4a26307ec8b28eb9c");
	CMonitoringDlg::InsertThreadBase(CMonitoringDlg::SUMMARY, m_threadSummaries);
	m_threadSummaries->RunThread();

	m_threadAccountStatus = make_shared<CBittrexAccountStatus>("002dbc500bea4b10a5d25b898dbc8ee1", "74796cf7e44842f4a26307ec8b28eb9c");
	m_threadAccountStatus->RunThread();

	shared_ptr<CBittrexOrderStatus> orderStatus = make_shared<CBittrexOrderStatus>("002dbc500bea4b10a5d25b898dbc8ee1", "74796cf7e44842f4a26307ec8b28eb9c");
	orderStatus->SetCallbackHandler(bind(&CScheduleManager::OrderStatusTradeCallback, CScheduleManager::GetInstance(), placeholders::_1));
	m_vecOrderStatus.push_back(orderStatus);
	orderStatus = make_shared<CBittrexOrderStatus>("002dbc500bea4b10a5d25b898dbc8ee1", "74796cf7e44842f4a26307ec8b28eb9c");
	orderStatus->SetCallbackHandler(bind(&CScheduleManager::OrderStatusTradeCallback, CScheduleManager::GetInstance(), placeholders::_1));
	m_vecOrderStatus.push_back(orderStatus);
	orderStatus = make_shared<CBittrexOrderStatus>("002dbc500bea4b10a5d25b898dbc8ee1", "74796cf7e44842f4a26307ec8b28eb9c");
	orderStatus->SetCallbackHandler(bind(&CScheduleManager::OrderStatusTradeCallback, CScheduleManager::GetInstance(), placeholders::_1));
	m_vecOrderStatus.push_back(orderStatus);

	for (auto &orderStatus : m_vecOrderStatus)
		orderStatus->RunThread();


	// test code
	vector<string> vecString;
	vecString.push_back("BTC-ETH");
	vecString.push_back("BTC-XRP");
	vecString.push_back("ETH-XRP");
	MakeOrderBookThread(vecString);

	for (auto &updater : m_vecThreadOnlyOrderBookUpdater)
		updater.second->RunThread();
} 

void CBittrexReceiver::MakeOrderBookThread(const vector<string> vecMarkets)
{
	for (auto marknetName : vecMarkets)
	{
		shared_ptr<CBittrexMarketOrderBookUpdater> updater = make_shared<CBittrexMarketOrderBookUpdater>("9ca3c3dfc0074e49b788af27ac2721a4", "237418116ee246c2957cf101693b73c6", marknetName);
		m_vecThreadOnlyOrderBookUpdater[marknetName] = updater;
	}
}


CBittrexReceiver::~CBittrexReceiver()
{

}

void CBittrexReceiver::Run()
{	
	
}

void CBittrexReceiver::Stop()
{
}

double CBittrexReceiver::GetMarketPrice(const string & sMarketName)
{
	response::MarketSummary summary = m_public->get_market_summary(sMarketName);
	return (double)summary.last;
}

int CBittrexReceiver::GetMarketList(CAbsMarketList & absMarketList)
{
	List<response::Market> markets = m_public->get_markets();

	auto& MarketNameTable = CBufferManager::GetInstance()->GetOrderBookBuffer()->GetMarketNameTable();
	if( MarketNameTable.size() == 0 ){
		CBufferManager::GetInstance()->GetReceiverBuffer()->m_orderBookNameBuffer.resize(markets.size());
		size_t nIndex = 0;
		for( auto& market : markets ){
			CBufferManager::GetInstance()->GetReceiverBuffer()->m_orderBookNameBuffer[nIndex] = (string)market.market_name;
			MarketNameTable[(string) market.market_name] = nIndex++;
		}

		CBufferManager::GetInstance()->GetOrderBookBuffer()->InitializeOrderBookQueue();
	}

	absMarketList.Initialize(markets.size());
	for (auto market : markets) {
		if (market.is_active) {
			CAbsMarket& NewMarket = absMarketList.InsertMarket((string)market.market_name, (string)market.base_currency, (string)market.market_currency, (double)market.min_trade_size);
		}
	}
	absMarketList.InitializeMarketPath();
	m_pAbsMarketList = &absMarketList;

	return 0;
}

int CBittrexReceiver::GetMarketSummary(CAbsMarketList & absMarketList)
{
	if (!absMarketList.IsInitialized()) {
		GetMarketList(absMarketList);
	}

	List<CAbsMarketSummary> marketSummaries = CBufferManager::GetInstance()->GetSummaryBuffer()->GetSummaries();

	for (auto& Summary : marketSummaries) {
		absMarketList.UpdateSummary(
			(string)Summary.market_name,
			(double)Summary.last,
			(double)Summary.bid,
			(double)Summary.ask,
			(string)Summary.time_stamp);
	}

	return (int) marketSummaries.size();
}
