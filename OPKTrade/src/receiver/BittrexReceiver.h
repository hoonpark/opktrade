#pragma once

#include "receiver.h"
#include "thread/ThreadOperatorBittrex.h"

//BittrexReceiver.h
class CBittrexReceiver : public CReceiver
{
public:
	CBittrexReceiver();
	virtual ~CBittrexReceiver();

	virtual void Run();
	virtual void Stop();

	virtual int GetMarketList(CAbsMarketList& absMarketList);
	virtual int GetMarketSummary(CAbsMarketList& absMarketList);

	virtual double GetMarketPrice(const string& sMarketName);

	void MakeOrderBookThread(const vector<string> vecMarkets);

private:
	shared_ptr<bittrex::Client> m_client;
	shared_ptr<bittrex::api::Public> m_public;

	CAbsMarketList* m_pAbsMarketList;
	vector<shared_ptr<bittrex::response::Market>> m_vPrimeMarket;
};
