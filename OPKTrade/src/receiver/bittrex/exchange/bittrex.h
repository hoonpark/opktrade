#pragma once

namespace bittrex
{
	namespace response
	{
		struct Market;
		struct OrderBook;
	};
	class Client;
};

