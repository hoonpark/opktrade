#pragma once

class CAbsMarket {
public:
	string m_sName;

public:
	//string market_name;
	string m_sMarketCurrency;
	string m_sBaseCurrency;

	//double market_price;
	double m_dMinSize;

	double m_dCurBid{ 0.0 };
	double m_dCurAsk{ 0.0 };
	double m_dLastPrice{ 0.0 };
	size_t m_nTimeStamp{ 0 };

	bool m_bChanged{ false };

public:
	void SetTimeStamp( const string& sTimeStamp );
	void UpdateMarket( double dLast, double dBid, double dAsk, const string & sTimeStamp );

public:
	CAbsMarket() = default;
	CAbsMarket( const string& sName ) : m_sName( sName ) {
	};
};

class CAbsTradePath {
public:
	vector<pair<size_t, bool>> m_vPathNode;		// size_t : index of market, bool : isBuyTrade

	string m_sPathName;
	double m_dYield{ 0.0 };

protected:

public:
	void CalcYield( vector<CAbsMarket>& vAbsMarket );
	static bool compareHelper( const CAbsTradePath& lhs, const CAbsTradePath& rhs ){ return lhs.m_dYield > rhs.m_dYield; }
	bool IsChangeStatus( vector<CAbsMarket>& vAbsMarket );
};

class CAbsMarketList {
protected:
	vector<CAbsMarket> m_vMarkets;

	map<string, size_t> m_mapMarketIndex;

	map<string, size_t> m_mapBTCMarketIndex;
	map<string, size_t> m_mapETHMarketIndex;
	map<string, size_t> m_mapUSDTMarketIndex;

	vector<CAbsTradePath> m_vTradePath;
	void SortTradePath(){ std::sort( m_vTradePath.begin(), m_vTradePath.end(), CAbsTradePath::compareHelper ); }

public:
	string & GetSuggestReport();
	vector<pair<string, bool>>& GetOrderBookRequest( const double dYieldCap );

public:
	CAbsMarketList() = default;

	bool IsInitialized(){ return m_vMarkets.size() > 0; }
	size_t GetMarketSize(){ return m_vMarkets.size(); }

	void Initialize( const size_t nMarketSize );
	size_t InitializeMarketPath();
	CAbsMarket& InsertMarket( const string& sMarketName );
	CAbsMarket& InsertMarket( const string& sMarketName, const string& sBaseCurrency, const string& sMarketCurrency, const double dMinSize );
	CAbsMarket* GetMarket( const string& sMarketName );		// TODO: 포인터 쓰는거 수정할 것

	bool UpdateSummary( const string& sMarketName, double dLast, double dBid, double dAsk, const string& sTimeStamp );

	double CalcYields();
};

