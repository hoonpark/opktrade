#include "stdafx.h"

#include "AbsMarket.h"

void CAbsMarketList::Initialize( const size_t nMarketSize )
{
	m_mapMarketIndex.clear();
	m_vMarkets.clear();
	m_vMarkets.reserve( nMarketSize );
}

size_t CAbsMarketList::InitializeMarketPath()
{
	size_t nSize = 0;

	size_t nUSDT_BTC = m_mapUSDTMarketIndex["BTC"];
	size_t nUSDT_ETH = m_mapUSDTMarketIndex["ETH"];
	size_t nBTC_ETH = m_mapBTCMarketIndex["ETH"];
	for( auto& usdtMarket : m_mapUSDTMarketIndex ){
		for( auto& btcMarket : m_mapBTCMarketIndex ){
			if( usdtMarket.first == btcMarket.first && usdtMarket.first != "ETH" ){
				nSize++;
				m_vTradePath.emplace_back( CAbsTradePath());
				{
					auto& NewPath = m_vTradePath.back();
					NewPath.m_vPathNode.emplace_back( make_pair( usdtMarket.second, true ) );
					NewPath.m_vPathNode.emplace_back( make_pair( btcMarket.second, false ) );
					NewPath.m_vPathNode.emplace_back( make_pair( nUSDT_BTC, false ) );
					NewPath.m_sPathName = "USDT-" + usdtMarket.first + "-BTC";
				}

				nSize++;
				m_vTradePath.emplace_back( CAbsTradePath() );
				{
					auto& NewPath = m_vTradePath.back();
					NewPath.m_vPathNode.emplace_back( make_pair( btcMarket.second, true ) );
					NewPath.m_vPathNode.emplace_back( make_pair( usdtMarket.second, false ) );
					NewPath.m_vPathNode.emplace_back( make_pair( nUSDT_BTC, true ) );
					NewPath.m_sPathName = "BTC-" + usdtMarket.first + "-USDT";
				}
				break;
			}
		}
		for( auto& ethMarket : m_mapETHMarketIndex ){
			if( usdtMarket.first == ethMarket.first ){
				nSize++;
				m_vTradePath.emplace_back( CAbsTradePath() );
				{
					auto& NewPath = m_vTradePath.back();
					NewPath.m_vPathNode.emplace_back( make_pair( usdtMarket.second, true ) );
					NewPath.m_vPathNode.emplace_back( make_pair( ethMarket.second, false ) );
					NewPath.m_vPathNode.emplace_back( make_pair( nUSDT_ETH, false ) );
					NewPath.m_sPathName = "USDT-" + usdtMarket.first + "-ETH";
				}

				nSize++;
				m_vTradePath.emplace_back( CAbsTradePath() );
				{
					auto& NewPath = m_vTradePath.back();
					NewPath.m_vPathNode.emplace_back( make_pair( ethMarket.second, true ) );
					NewPath.m_vPathNode.emplace_back( make_pair( usdtMarket.second, false ) );
					NewPath.m_vPathNode.emplace_back( make_pair( nUSDT_ETH, true ) );
					NewPath.m_sPathName = "ETH-" + usdtMarket.first + "-USDT";
				}
				break;
			}
		}
	}
	for( auto& btcMarket : m_mapBTCMarketIndex ){
		for( auto& ethMarket : m_mapETHMarketIndex ){
			if( btcMarket.first == ethMarket.first ){
				nSize++;
				m_vTradePath.emplace_back( CAbsTradePath() );
				{
					auto& NewPath = m_vTradePath.back();
					NewPath.m_vPathNode.emplace_back( make_pair( btcMarket.second, true ) );
					NewPath.m_vPathNode.emplace_back( make_pair( ethMarket.second, false ) );
					NewPath.m_vPathNode.emplace_back( make_pair( nBTC_ETH, false ) );
					NewPath.m_sPathName = "BTC-" + btcMarket.first + "-ETH";
				}

				nSize++;
				m_vTradePath.emplace_back( CAbsTradePath() );
				{
					auto& NewPath = m_vTradePath.back();
					NewPath.m_vPathNode.emplace_back( make_pair( ethMarket.second, true ) );
					NewPath.m_vPathNode.emplace_back( make_pair( btcMarket.second, false ) );
					NewPath.m_vPathNode.emplace_back( make_pair( nBTC_ETH, true ) );
					NewPath.m_sPathName = "ETH-" + btcMarket.first + "-BCT";
				}
				break;
			}
		}
	}
	return nSize;
}

CAbsMarket & CAbsMarketList::InsertMarket( const string & sMarketName )
{
	CAbsMarket& NewMarket = CAbsMarket( sMarketName );
	m_vMarkets.emplace_back( NewMarket );
	m_mapMarketIndex[sMarketName] = m_vMarkets.size() - 1;

	return m_vMarkets.back();
}

CAbsMarket & CAbsMarketList::InsertMarket( const string & sMarketName, const string & sBaseCurrency, const string & sMarketCurrency, const double dMinSize )
{
	size_t nCurIndex = m_vMarkets.size();

	m_vMarkets.emplace_back( CAbsMarket( sMarketName ) );

	CAbsMarket& NewMarket = m_vMarkets.back();
	m_mapMarketIndex[sMarketName] = nCurIndex;

	NewMarket.m_sBaseCurrency = sBaseCurrency;
	NewMarket.m_sMarketCurrency = sMarketCurrency;
	NewMarket.m_dMinSize = dMinSize;

	// update Base market list
	if( sBaseCurrency == "USDT" ){
		m_mapUSDTMarketIndex[sMarketCurrency] = nCurIndex;
	} else if( sBaseCurrency == "BTC" ){
		m_mapBTCMarketIndex[sMarketCurrency] = nCurIndex;
	} else if( sBaseCurrency == "ETH" ){
		m_mapETHMarketIndex[sMarketCurrency] = nCurIndex;
	}

	return NewMarket;
}

CAbsMarket * CAbsMarketList::GetMarket( const string & sMarketName )
{
	if( m_mapMarketIndex.find( sMarketName ) != m_mapMarketIndex.end() ){
		return &m_vMarkets[m_mapMarketIndex[sMarketName]];
	}
	return nullptr;
}

bool CAbsMarketList::UpdateSummary( const string & sMarketName, double dLast, double dBid, double dAsk, const string & sTimeStamp )
{
	if( m_mapMarketIndex.find( sMarketName ) != m_mapMarketIndex.end() ){
		auto& CurMarket = m_vMarkets[m_mapMarketIndex[sMarketName]];
		CurMarket.UpdateMarket( dLast, dBid, dAsk, sTimeStamp );
		//CurMarket.m_dCurBid = dBid;
		//CurMarket.m_dCurAsk = dAsk;
		//CurMarket.m_dLastPrice = dLast;
		//CurMarket.SetTimeStamp( sTimeStamp );

		return true;
	}

	return false;
}

double CAbsMarketList::CalcYields()
{
	double dResult = -1.0;
	
	for( auto& singlePath : m_vTradePath ){
		singlePath.CalcYield( m_vMarkets );
		if( dResult < singlePath.m_dYield ) dResult = singlePath.m_dYield;
	}
	SortTradePath();

	return dResult;
}

string & CAbsMarketList::GetSuggestReport()
{
	// TODO: 여기에 반환 구문을 삽입합니다.
	static string sReport;
	stringstream ss;

	ss.clear();

	for( auto& singlePath : m_vTradePath ){
		ss << singlePath.m_sPathName << " : " << setprecision(3) << ( singlePath.m_dYield - 1.0 ) * 100 << "%" << endl;
	}
	sReport = ss.str();

	return sReport;
}

vector<pair<string, bool>>& CAbsMarketList::GetOrderBookRequest( const double dYieldCap )
{
	static vector<pair<string, bool>> vRequest;

	vRequest.clear();

	for( auto& tradePath : m_vTradePath ){
		if( ( ( tradePath.m_dYield - 1.0 ) * 100 ) < dYieldCap ) break;
		// USDT Market blocking
		//if( m_vMarkets[tradePath.m_vPathNode[0].first].m_sBaseCurrency == "USDT" )
		//	continue;

		if( tradePath.IsChangeStatus( m_vMarkets ) ){
			vRequest.emplace_back( make_pair( tradePath.m_sPathName, true ) );
			for( auto& singlePath : tradePath.m_vPathNode ){
				vRequest.emplace_back( make_pair( m_vMarkets[singlePath.first].m_sName, singlePath.second ) );
			}
			vRequest.emplace_back( make_pair( "<E>", true ) );
		}
	}

	return vRequest;
}

void CAbsMarket::SetTimeStamp( const string & sTimeStamp )
{
	m_nTimeStamp = 0;
	for( auto& c : sTimeStamp ){
		if( c == '.' ) break;
		if( c >= '0' && c <= '9' ){
			m_nTimeStamp = m_nTimeStamp * 10 + c - '0';
		}
	}
}

void CAbsMarket::UpdateMarket( double dLast, double dBid, double dAsk, const string & sTimeStamp )
{
	m_bChanged = ( m_dCurBid != dBid || m_dCurAsk != dAsk || m_dLastPrice != dLast );

	m_dCurBid = dBid;
	m_dCurAsk = dAsk;
	m_dLastPrice = dLast;

	SetTimeStamp( sTimeStamp );
}

void CAbsTradePath::CalcYield( vector<CAbsMarket>& vAbsMarket )
{
	vector<double> vPrice;
	vPrice.resize( m_vPathNode.size() );

	size_t i = 0;
	bool bLastMarketDirection = true;
	for( auto& pair : m_vPathNode ){
		auto& market = vAbsMarket[pair.first];
		vPrice[i++] = pair.second ? market.m_dCurAsk : market.m_dCurBid;
		bLastMarketDirection = pair.second;
	}

	auto CalcYieldTrianbleTrade = [&]( bool bDir ) -> double {
		if( bDir )
			return vPrice[1] / ( vPrice[0] * vPrice[2] );

		return vPrice[1] * vPrice[2] / vPrice[0];
	};

	m_dYield = CalcYieldTrianbleTrade( bLastMarketDirection );
}

bool CAbsTradePath::IsChangeStatus( vector<CAbsMarket>& vAbsMarket )
{
	bool bResult = false;
	for( auto& pair : m_vPathNode ){
		auto& market = vAbsMarket[pair.first];
		bResult |= market.m_bChanged;
	}
	return bResult;
}
