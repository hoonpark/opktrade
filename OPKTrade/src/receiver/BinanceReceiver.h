#pragma once
#include "receiver.h"
class CBinanceReceiver : public CReceiver
{
public:
	CBinanceReceiver();

	virtual void Run();
	virtual void Stop();

	virtual int GetMarketList(CAbsMarketList& absMarketList);
	virtual int GetMarketSummary(CAbsMarketList& absMarketList);

	virtual double GetMarketPrice(const string& sMarketName);
};
