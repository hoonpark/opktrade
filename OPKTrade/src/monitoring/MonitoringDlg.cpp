// OPKTradeDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "OPKTrade.h"
#include "MonitoringDlg.h"
#include "afxdialogex.h"
#include <iostream>
#include "resource.h"
#include "analyzer/analyzer.h"
#include "scheduler/Scheduler.h"
#include "scheduler/BufferManager.h"
#include "../../define.h"

#include "receiver/BittrexReceiver.h"
#include "receiver/binance/binacpp.h"
#include "../receiver/binance/binacpp_websocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange( CDataExchange* pDX );    // DDX/DDV 지원입니다.

														// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx( IDD_ABOUTBOX )
{
}

void CAboutDlg::DoDataExchange( CDataExchange* pDX )
{
	CDialogEx::DoDataExchange( pDX );
}

BEGIN_MESSAGE_MAP( CAboutDlg, CDialogEx )
END_MESSAGE_MAP()

// CMonitoringDlg 대화 상자


map<int, shared_ptr<CThreadOperatorBase>> CMonitoringDlg::m_mapThreads;


CMonitoringDlg::CMonitoringDlg( CWnd* pParent /*=nullptr*/ )
	: CDialogEx( IDD_OPKTRADE_DIALOG, pParent )
{
	m_hIcon = AfxGetApp()->LoadIcon( IDR_MAINFRAME );

	m_pAnalyzer = make_shared<CAnalyzer>();
	CDebugLog::m_pDebugLogView = &m_listboxLogView;
	CDebugLog::m_pTradeLogView = &m_listboxTradeLogView;
}

map<int, shared_ptr<CThreadOperatorBase>>& CMonitoringDlg::GetThreadBase()
{
	return m_mapThreads;
}

void CMonitoringDlg::InsertThreadBase( int index, shared_ptr<CThreadOperatorBase> thread )
{
	if( m_mapThreads.find( index ) != m_mapThreads.end() ) return;

	m_mapThreads[index] = thread;
}

void CMonitoringDlg::DoDataExchange( CDataExchange* pDX )
{
	CDialogEx::DoDataExchange( pDX );
	DDX_Control( pDX, IDC_LIST_LOGVIEW, m_listboxLogView );
	DDX_Control( pDX, IDC_LIST_TRADELOGVIEW, m_listboxTradeLogView );
}

BEGIN_MESSAGE_MAP( CMonitoringDlg, CDialogEx )
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

	ON_MESSAGE( WM_USER_UPDATE_MARKET_LIST, OnUpdateMarketList )
	ON_MESSAGE( WM_USER_UPDATE_MARKET_SUMMARIES, OnUpdateMarketSummaries )
	ON_MESSAGE( WM_USER_UPDATE_ORDER_BOOKS, OnUpdateMarketOrderBooks )
	ON_BN_CLICKED( IDC_BTN_START_TRADE, &CMonitoringDlg::OnBnClickedBtnStartTrade )
	ON_BN_CLICKED( IDC_BTN_DBG_LOG, &CMonitoringDlg::OnBnClickedBtnDbgLog )
END_MESSAGE_MAP()


// COPKTradeDlg 메시지 처리기


map < string, map <double, double> >  depthCache;
int lastUpdateId;

//------------------------------
void print_depthCache() {

	map < string, map <double, double> >::iterator it_i;

	for( it_i = depthCache.begin(); it_i != depthCache.end(); it_i++ ) {

		string bid_or_ask = ( *it_i ).first;
		cout << bid_or_ask << endl;
		cout << "Price             Qty" << endl;

		map <double, double>::reverse_iterator it_j;

		for( it_j = depthCache[bid_or_ask].rbegin(); it_j != depthCache[bid_or_ask].rend(); it_j++ ) {

			double price = ( *it_j ).first;
			double qty = ( *it_j ).second;
			printf( "%.08f          %.08f\n", price, qty );
		}
	}
}

//-------------
int ws_depth_onData( Json::Value &json_result ) {

	int i;

	int new_updateId = json_result["u"].asInt();

	if( new_updateId > lastUpdateId ) {

		for( i = 0; i < (int) json_result["b"].size(); i++ ) {
			double price = atof( json_result["b"][i][0].asString().c_str() );
			double qty = atof( json_result["b"][i][1].asString().c_str() );
			if( qty == 0.0 ) {
				depthCache["bids"].erase( price );
			} else {
				depthCache["bids"][price] = qty;
			}
		}
		for( i = 0; i < (int) json_result["a"].size(); i++ ) {
			double price = atof( json_result["a"][i][0].asString().c_str() );
			double qty = atof( json_result["a"][i][1].asString().c_str() );
			if( qty == 0.0 ) {
				depthCache["asks"].erase( price );
			} else {
				depthCache["asks"][price] = qty;
			}
		}
		lastUpdateId = new_updateId;
	}
	print_depthCache();
	return 0;
}

BOOL CMonitoringDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT( ( IDM_ABOUTBOX & 0xFFF0 ) == IDM_ABOUTBOX );
	ASSERT( IDM_ABOUTBOX < 0xF000 );

	CMenu* pSysMenu = GetSystemMenu( FALSE );
	if( pSysMenu != nullptr )
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString( IDS_ABOUTBOX );
		ASSERT( bNameValid );
		if( !strAboutMenu.IsEmpty() )
		{
			pSysMenu->AppendMenu( MF_SEPARATOR );
			pSysMenu->AppendMenu( MF_STRING, IDM_ABOUTBOX, strAboutMenu );
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다. 
	SetIcon( m_hIcon, TRUE );			// 큰 아이콘을 설정합니다.
	SetIcon( m_hIcon, FALSE );		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	OnBnClickedBtnDbgLog();

	/*string key = "JCoHX3cg0CLwBVg5LUR+1V2TXfuFgu4LlVQZvmBjIsVEBpmi76aa3SnUtrGkKL4HG";
	string secret = "M8roU7HFc9bvX5m4HFr3VuhsP2msJBjN9tAftoYL5hXrqlT29lzNNvvKkANM7Qd9";

	BinaCPP b;
	b.init( key, secret );

	Json::Value result;
	b.get_depth("XRPETH", 10, result);*/
	//b.get_allBookTickers( result );


	//b.get_serverTime(result);
	//double d = b.get_price("XRPETH");

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMonitoringDlg::OnSysCommand( UINT nID, LPARAM lParam )
{
	if( ( nID & 0xFFF0 ) == IDM_ABOUTBOX )
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	} else
	{
		CDialogEx::OnSysCommand( nID, lParam );
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMonitoringDlg::OnPaint()
{
	if( IsIconic() )
	{
		CPaintDC dc( this ); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage( WM_ICONERASEBKGND, reinterpret_cast<WPARAM>( dc.GetSafeHdc() ), 0 );

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics( SM_CXICON );
		int cyIcon = GetSystemMetrics( SM_CYICON );
		CRect rect;
		GetClientRect( &rect );
		int x = ( rect.Width() - cxIcon + 1 ) / 2;
		int y = ( rect.Height() - cyIcon + 1 ) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon( x, y, m_hIcon );
	} else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMonitoringDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>( m_hIcon );
}




void CMonitoringDlg::Refresh( const string &msg )
{
	GetDlgItem( IDC_STC_MONITORING )->SetWindowText( msg.c_str() );
}


LRESULT CMonitoringDlg::OnUpdateMarketList( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

LRESULT CMonitoringDlg::OnUpdateMarketSummaries( WPARAM wParam, LPARAM lParam )
{
	auto MarketSummaries = CBufferManager::GetInstance()->GetSummaryBuffer()->GetSummaries();
	auto dMaxYield = m_pAnalyzer->SimpleCheckYield();
	double dYieldCutline = 0.75;

	auto CalcYield2Percent = []( const double dYield ) -> double {
		return ( ( dYield - 1.0 ) * 100 );
	};

	if( CalcYield2Percent( dMaxYield ) > dYieldCutline ){
		// 수익이 예상되는 경우에 대한 Test
		auto vRequest = m_pAnalyzer->GetOrderBookRequest( dYieldCutline );
		CTradePlan tradePlan;
		bool bPlanStart;
		bool bCheckOnLoop{ false };

		bPlanStart = true;
		for( auto& request_data : vRequest ){
			if (!CBufferManager::GetInstance()->GetReceiverBuffer()->CheckOnTrading(request_data.first) && !CBufferManager::GetInstance()->GetReceiverBuffer()->IsExistPlan(request_data.first)) {
				if( bCheckOnLoop ){
					if( request_data.first == "<E>" ){
						bCheckOnLoop = false;
					}
				} else {
					if( bPlanStart ){
						tradePlan.Initialize();
						tradePlan.m_sTradePath = request_data.first;
						bPlanStart = false;
					} else if( request_data.first == "<E>" ){
						CBufferManager::GetInstance()->GetReceiverBuffer()->SetTradePlan( tradePlan );
						bPlanStart = true;
					} else {
						CTradePlan::CTradePlanNode planNode;
						CBufferManager::GetInstance()->GetReceiverBuffer()->WriteOrderRequest(request_data.first, request_data.second);
						planNode.m_sMarketName = request_data.first;
						planNode.m_bBuy = request_data.second;
						tradePlan.m_vPlan.emplace_back( planNode );
					}
				}
			} else {
				bCheckOnLoop = true;
			}
		}
	}

	GetDlgItem( IDC_STC_MONITORING )->SetWindowText( m_pAnalyzer->GetSuggestReport().c_str() );

	CBufferManager::GetInstance()->GetSummaryBuffer()->ClearSummaries();
	return 0L;
}

LRESULT CMonitoringDlg::OnUpdateMarketOrderBooks( WPARAM wParam, LPARAM lParam )
{
	int nIndex = (int) wParam;

	auto& mapNameTable = CBufferManager::GetInstance()->GetOrderBookBuffer()->GetMarketNameTable();
	string marketName = CBufferManager::GetInstance()->GetReceiverBuffer()->m_orderBookNameBuffer[nIndex];
	auto OrderBookQueue = CBufferManager::GetInstance()->GetOrderBookBuffer()->GetOrderBookBufferQueue( marketName );

	for( auto& orderBook : OrderBookQueue ){
		CBufferManager::GetInstance()->GetReceiverBuffer()->UpdateTradePlan(marketName, orderBook);
	}

	CBufferManager::GetInstance()->GetOrderBookBuffer()->ClearOrderBook(marketName);
	return 0L;
}

void CMonitoringDlg::OnBnClickedBtnStartTrade()
{
	stringstream ss;
	ss << "Starting Trader..." << endl;
	DebugLog.Write( ss.str() );

	m_pAnalyzer->run();
}


void CMonitoringDlg::OnBnClickedBtnDbgLog()
{
	m_listboxLogView.ResetContent();
	m_listboxTradeLogView.ResetContent();

	//if( m_pDebugLogDlg.get() == nullptr ){
	//	m_pDebugLogDlg = make_shared<CDebugLogDlg>();
	//	m_pDebugLogDlg->Create( IDD_DLG_DEBUG_LOG );
	//	CDebugLog::m_pDebugLogDlg = m_pDebugLogDlg;
	//}
	//if( m_pDebugLogDlg->IsWindowVisible() ){
	//	m_pDebugLogDlg->ShowWindow( SW_HIDE );
	//} else {
	//	m_pDebugLogDlg->ShowWindow( SW_SHOW );
	//}
}


void CMonitoringDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::OnOK();
}


void CMonitoringDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::OnCancel();
}
