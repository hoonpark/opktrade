#pragma once
#include "debug/DebugLogDlg.h"

class CDebugLogDlg;
class CAnalyzer;
class CThreadOperatorBase;


// COPKTradeDlg 대화 상자
class CMonitoringDlg : public CDialogEx
{
	// 생성입니다.
public:
	enum THREAD_TYPE
	{
		TRADE = 0,
		SUMMARY
	};
	CMonitoringDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.
	static void InsertThreadBase(int index, shared_ptr<CThreadOperatorBase> thread);
	static map<int, shared_ptr<CThreadOperatorBase>>& GetThreadBase();
											// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OPKTRADE_DIALOG };
#endif

	void Refresh(const string &msg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


														// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	shared_ptr<CAnalyzer> m_pAnalyzer;
	bool m_bRefreshMarket{ false };
	static map<int, shared_ptr<CThreadOperatorBase>> m_mapThreads;

public:
	CDebugLogView m_listboxLogView;
	CDebugLogView m_listboxTradeLogView;
	//void AddLog( const string& sLog );

public:
	afx_msg LRESULT OnUpdateMarketList(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateMarketSummaries(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateMarketOrderBooks(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedBtnStartTrade();
	afx_msg void OnBnClickedBtnDbgLog();
	virtual void OnOK();
	virtual void OnCancel();
};
