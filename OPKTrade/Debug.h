#pragma once

//class CMonitoringDlg;

class CDebugLogView;
class CDebugLog{
private:
	string sLogFilePath{ "c:\\temp\\new.txt" };
	string sTradeLogFilePath{ "c:\\temp\\tradeLog.txt" };
	string sHiddenLogFilePath{ "c:\\temp\\hiddenLog.txt" };

protected:
	string & GetTimeStamp();
public:
	static CDebugLogView* m_pDebugLogView;
	static CDebugLogView* m_pTradeLogView;
	CDebugLog() = default;
	//{
	//	sLogFilePath = "c:\\temp\\new.txt";
	//	sTradeLogFilePath = "c:\\temp\\tradeLog.txt";
	//}

public:
	void Write( const string& logText );
	void HiddenWrite( const string& logText );
	void TradeLog( const string& logText );
};

extern CDebugLog DebugLog;
